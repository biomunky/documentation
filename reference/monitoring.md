# Monitoring

#### Enable JMX

Apply these settings to the joule execution path

```
-Dcom.sun.management.jmxremote.port=5000
-Dcom.sun.management.jmxremote.rmi.port=5000
```

For further information use the instructions in [Monitoring and Management Using JMX Technology](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html)
