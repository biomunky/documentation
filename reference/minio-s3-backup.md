# MinIO S3 Backup

The MinIO publisher transport publishes batches of events as formatted files on to configured S3 local or cloud based buckets.

#### _Driver details_

```groovy
io.minio:minio:8.5.4
```

#### Example configuration

```yaml
minioPublisher:
  name: "marketdata-S3Publisher"

  connection:
    endpoint: "https://localhost"
    port: 9000
    tls: false
    credentials:
      access key: "xxxx"
      secret key: "yyyy"

  bucket persistence:
    bucketId: "marketdata"
    object name: "marketdataPublisher"
    date format: "yyyyMMdd/HH"
    versioning: ENABLED
    retries: 3

    formatter:
      parquet formatter:
        contentType: "binary/octet-stream"
        encoding: "UTF-8"
        schema: "./marketdata.avro"
        compression codec: SNAPPY
        temp directory: "./tmp"

  batchSize: 100000
```

The above example uses a local MinIO platform where by a configured S3 bucket receives compressed batched parquet formatted files.

### Core Attributes

Configuration parameters available for the MinIO publisher transport. The parameters are organised by order of importance, ranked from high to low.

<table><thead><tr><th width="166">Attribute</th><th width="217">Description</th><th width="246">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>connection </td><td>Connection setting to hosted service</td><td>See <a href="minio-s3-backup.md#connection-attributes">Connection Attributes</a> section</td><td>true</td></tr><tr><td>bucket persistence</td><td>Bucket persistence configurations</td><td>See <a href="minio-s3-backup.md#bucket-attributes">Bucket Attributes</a> section</td><td>true</td></tr><tr><td>batchSize</td><td>Number of events to batch send, maps to batch.size. Batch size of zero disables batching function</td><td><p>Integer</p><p>Default: 100000</p></td><td>false</td></tr></tbody></table>

## **Connection** Attributes

This topic provides configuration parameters available for the connection section.&#x20;

<table><thead><tr><th width="161">Attribute</th><th width="220">Description</th><th width="259">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>region</td><td>Target region for S3 bucket hosting</td><td>String</td><td>false</td></tr><tr><td>endpoint</td><td>Service address i.e.<br><a href="https://storage.googleapis.com">https://storage.googleapis.com</a></td><td><p>String</p><p>Default: https://localhost </p></td><td>true</td></tr><tr><td>port</td><td>Port service is hosted on. This can be provided within the endpoint setting. Valid range is between 1025 to 65536</td><td>Integer<br>Default: 9000</td><td>false</td></tr><tr><td>tls</td><td>Use TLS security</td><td>Boolean<br>Default: false</td><td>false</td></tr><tr><td>credentials</td><td>The credentials to use to login to the platform</td><td>See <a href="minio-s3-backup.md#credentials-attributes">Credentials Attributes</a></td><td>true</td></tr></tbody></table>

### **Credentials** Attributes

This topic provides configuration parameters available for the credentials section.&#x20;

<table><thead><tr><th width="161">Attribute</th><th width="220">Description</th><th width="259">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>access key</td><td>Client access key</td><td>String</td><td>true</td></tr><tr><td>secret key</td><td>Client secret key</td><td>String </td><td>true</td></tr><tr><td>provider plugin</td><td>Custom plugin that provides a MinIO provider class implementation</td><td>String<br>e.g. Full package and class name</td><td>true</td></tr></tbody></table>

If a provider plugin has been provided the access and secret keys are not required

### Custom Credential Providers

For production based solutions credentials need to be provided by a secure environment. Joule provides a `JouleProviderPlugin` interface for custom implementations.

#### **Example**

```yaml
minioPublisher:
  name: "marketdata-S3Publisher"

  connection:
    endpoint: "https://localhost"
    port: 9000
    tls: true
    credentials:
      provider plugin: com.fractalworks.streams.transport.minio.credentials.JWTCredentialsProvider
```

#### Custom Provider implementation&#x20;

The complete implementation of this class would provide MinIO with a custom credentials Provider instance required of an enterprise production solution. See MinIO [Github](https://github.com/minio/minio-java/blob/master/examples/MinioClientWithWebIdentityProvider.java) for implementation examples.

```java
public class JWTCredentialsProvider implements JouleProviderPlugin {
    @Override
    public Provider getProvider() {
        // TODO: Implementation
        return null;
    }

    @Override
    public void initialize() throws CustomPluginException {
        // TODO: Implementation
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        // TODO: Implementation
    }

    @Override
    public void setProperties(Properties properties) {
        // TODO: Implementation
    }
}
```

## Bucket Attributes

<table><thead><tr><th width="161">Attribute</th><th width="220">Description</th><th width="259">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bucketId</td><td>Bucket name</td><td>String</td><td>true</td></tr><tr><td>object name</td><td>File name to be used</td><td>String </td><td>true</td></tr><tr><td>formatter</td><td>Formatter to use to convert StreamEvents to target data formatter</td><td>See <a href="../connector-catalog/serialization/serializers.md">Formatters documentation</a><br>Default: csv formatter</td><td>false</td></tr><tr><td>versioning</td><td>Keep multiple variants of an object in the same <em>bucket</em><br><br><em>Valid values: ENABLED, SUSPENDED</em></td><td>String<br>Default: ENABLED</td><td>false</td></tr><tr><td>bucket policy</td><td>Bucket policy to use. Provide path and file to policy</td><td>String</td><td>false</td></tr><tr><td>partition by date</td><td>Create bucket directories based upon date </td><td>Boolean<br>Default: True</td><td>false</td></tr><tr><td>date format</td><td>Date format to use when enabling <em>"partition by date"</em> feature</td><td>String<br>Default: yyyyMMdd</td><td>false</td></tr><tr><td>custom directory</td><td>Specify a custom defined directory path for files to be placed within</td><td>String</td><td>false</td></tr><tr><td>headers</td><td>File header information</td><td>Map&#x3C;String,String></td><td>false</td></tr><tr><td>user metadata</td><td>User meta data to be applied to files</td><td>Map&#x3C;String,String></td><td>false</td></tr><tr><td>retries</td><td>Number of time to try to publish file</td><td>Integer<br>Default: 3</td><td>false</td></tr></tbody></table>
