# SQL

Joule ships with an embedded in-memory modern SQL engine, [DuckDB](https://duckdb.org). This is used to capture events flowing through the processing pipeline along with supporting the metrics engine implementation.

## Key Features

* Event [Tap](../processors/event-tap.md) for event capture and storage
* [Metrics Engine](../processors/analytics/metrics-engine/) to provide SQL analytics
* Data access via the [Rest API](../joule-sdk/rest-database-endpoints.md)&#x20;
