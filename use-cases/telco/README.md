# 📱 Telco

## Overview <a href="#overview" id="overview"></a>

The objective of this project is provide a set of examples that demonstrates some of the core Joule platform capabilities. Each use case builds upon the previous to represent how use cases can be built and tested incrementally with the final use case combining all functionality to demonstrate a simple direct telco marketing campaign using simulated streaming mobile events and geospatial analytics.

A set of use case examples have been defined as a sequence to gain how incremental functionality is developed for a target use case. We start from the basic filtering of user events to direct marketing using real-time geospatial triggers. All use cases source event data from a basic mobile event simulator described further in the document.

1. IMSI Opt-out filtering
2. Enrich events with device information
3. Anonymise IMSI field
4. Trigger location tracking using predefined geofences
5. Generate geospatial marketing messages based on location

{% hint style="info" %}
**Good to know:** depending on the product you're building, it can be useful to explicitly document use cases. Got a product that can be used by a bunch of people in different ways? Maybe consider splitting it out!
{% endhint %}

### Setting up

{% tabs %}
{% tab title="Install" %}
Clone the [project repository](https://gitlab.com/joule-platform/fractalworks-telco-example) is hosted on Gitlab
{% endtab %}

{% tab title="Build" %}
By building the example locally you will have the flexibility to create new use cases, analytics, processors, transports and contribute back the to project. Build the project in the root directory by following the below instructions.

First build the project

<pre class="language-bash"><code class="lang-bash">gradle clean build &#x26;&#x26; cd build/distributions 
<strong>    &#x26;&#x26; unzip fractalworks-telco-example-1.0.zip 
</strong>    &#x26;&#x26; cd fractalworks-telco-example-1.0 
    &#x26;&#x26; chmod +x bin/*.sh
</code></pre>
{% endtab %}

{% tab title="Run" %}
Start joule processor

```bash
./bin/startJoule.sh
```

Events will be generated, published, consumed, process and finally published to InfluxDB and ready to be visualised in Grafana.
{% endtab %}
{% endtabs %}

### Components <a href="#components" id="components"></a>

The following Joule platform components are used

### Processors <a href="#processors" id="processors"></a>

* Filtering
* Anonymisation
* Geospatial
* Joule SDK for custom plugins

### Integrations <a href="#integrations" id="integrations"></a>

* Geode, caching layer to provide low latency data to processing clients, integrated to Postgres
* Postgres, database holding reference data and hosted with in a Docker container
* InfluxDB, time series database for processed events

### Joule Image <a href="#joule-image" id="joule-image"></a>

The latest Joule image can be pulled from the following [docker hub link](https://hub.docker.com/r/fractalworks/joule)

## Event Simulator <a href="#event-simulator" id="event-simulator"></a>

Events for these examples are generated using a simulator, events are modelled on simple mobile events. The simulator has been developed using the Joule SDK by extending the `AbstractConsumerTransport` class.

Events are generated for five IMSIs to simulate entities moving through London streets. The default journey provided has IMSIs travelling from Waterloo station to St Pauls and back until the example process is stopped Each IMSI speed and direction is dynamically set on each movement iteration to simulate the general movement of entities in the system. Along with entity movement, the algorithm sets the corresponding connected cell tower, geospatial coordinates, data usage and dropped calls.

### Event Structure <a href="#event-structure" id="event-structure"></a>

The following fields are available on each event

```
- ingestTime
- eventTime
- imsi
- imei
- bundleid
- bytesUp
- bytesDown
- epsAttachAttempts
- smsMo2gFailRate
- droppedCall
- latlng
- celltower
```

### **Note**

You can provide your own journey path, see the below example, and geofences, see further down in the document

```bash
cat conf/sources/mobileSimulatedLondonTubeTripStream.yaml
```
