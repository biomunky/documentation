---
description: >-
  Leverage location tracking by adding geo triggers along with custom marketing
  messaging we can open up new market opportunities that deliver custom customer
  experiences.
---

# Geospatial marketing

This example simulates geospatial real-time marketing through the use of geofence triggers and custom messages. Geofences are automatically added to the processor through the use of auto reference data binding.

## Use case configuration

_File: app-geospatialMarketingCampaign.env_

```bash
SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=${PWD}/conf/sources/mobileReferenceData.yaml
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventDynamicCampaignStream.yaml
PUBLISHFILE=${PWD}/conf/publishers/marketingCampaignFile.yaml
```

Reference data stores can be used to prime the geofences. Change the required yaml file to test this feature in the app.env file.

```bash
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventDynamicCampaignUsingGeofenceDataStoreStream.yaml
```

## Pipeline **configuration**

{% code overflow="wrap" %}
```yaml
processing unit:
  pipeline:
    - filter:
        expression: "(imsi !== null ) ? true : false;"
    
    - entity geo tracker:
        name: TFLMessage
        entity key: imsi
        min dwelling time: 2
        timeUnit: SECONDS  
        default radius: 150.0f
        geofences:
          stores:
            geoFenceStore:
              storeName: londonTransportStations
              getInitialImage: true
              initialImageQuery: select * from londonTransportStations
              primaryKey: id
        
    - geofence occupancy trigger:
        name: marketingCampaign
        tracker field: geoTrackingInfo
        plugin: com.fractalworks.streams.examples.telco.marketing.MarketingCampaignMessenger
    
    - filter:
        expression: "(typeof campaignMsg !== 'undefined' && campaignMsg !== null) ? true : false;"
```
{% endcode %}

### Output Event

The following fields are added to the processed StreamEvent object.

```
// Some code

```
