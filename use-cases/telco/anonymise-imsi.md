---
description: >-
  In-flight data anonymisation is a key feature for solutions that process
  sensitive  data. Joule provides this feature through a custom encryption
  obfuscator.
---

# Anonymise IMSI

This example performs field level anonymisation on the customer IMSI using RSA encryption. Encryption keys are dynamically generated from a random  initialisation vector with the resulting keys stored in a user defined directory. Encryption processor Documentation can be found [here](../../processors/transformation/obfuscation/encryption.md).

## Use case configuration

_File: app-anonymisation.env_

```bash
SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
ENGINEFILE=${PWD}/conf/usecases/anonymisation/mobileEventAnonymisation.yaml
PUBLISHFILE=${PWD}/conf/publishers/anonymisedMobileEventFile.yaml
```

## Pipeline configuration <a href="#processor-configuration" id="processor-configuration"></a>

This pipeline will encrypt the event IMSI using sytem generated keys.

```yaml
processing unit:
  pipeline:
    - filter:
          expression: "(imsi !== null ) ? true : false;"
         
    - obfuscation:
        name: imsiEncryption
        fields:
          imsi:
            encryption:
              decrypt: false
              key location: ./keytmp
              aes bit length: 128
              rsa bit length: 2048
              rotate keys: true
```
