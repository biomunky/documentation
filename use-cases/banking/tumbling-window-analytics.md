---
description: >-
  Tumbling windows are useful to gain fixed analytic insights over a period of
  either time-based or a fixed count of events.
---

# Tumbling window analytics

This example provides a demonstration of the available OOTB aggregate functions Joule provides that work with tumbling windows.

## Use case configuration

_File: app-tumblingWindowAnalytics.env_

```bash
SOURCEFILE=${PWD}/conf/sources/stockQuoteStream.yaml
ENGINEFILE=${PWD}/conf/usecases/tumblingWindowAnalytics.yaml
PUBLISHFILE=${PWD}/conf/publishers/fileStandardAnalytics.yaml
```

## Pipeline configuration <a href="#processor-configuration" id="processor-configuration"></a>

This pipeline will perform two functions. Firstly store every event in the in-memory database and then build 5-second tumbling windows resulting in the defined aggregate functions being executed over the event

{% code overflow="wrap" %}
```yaml
processing unit:
  pipeline:
    - time window:
        emitting type: tumblingQuoteAnalytics
        aggregate functions:
          FIRST: [ask]
          LAST: [ ask ]
          MIN: [ ask ]
          MAX: [ bid ]
          SUM: [ volume ]
          MEAN: [ volatility ]
          HARMONIC_MEAN: [ volatility ]
          VARIANCE: [ volatility ]
          STDEV: [ volatility ]
        policy:
          type: tumblingTime
          window size: 5000

select:
  expression: "symbol, ask_FIRST, ask_LAST, ask_MIN, bid_MAX, volume_SUM, volatility_MEAN, volatility_HARMONIC_MEAN, volatility_VARIANCE, volatility_STDEV"

group by:
  - symbol
```
{% endcode %}

