---
description: >-
  Combining aggregates, stateful and stateless window functions is a powerful
  mix to provide group-by analytics over a stream of events
---

# Aggregates and user defined time based window functions

This example computes basic aggregates and a mix of stateless and stateful analytic functions or time base sliding windows.

## Use case configuration

_File: app-aggregatesAndEMA.env_

```bash
SOURCEFILE=${PWD}/conf/sources/stockQuoteStream.yaml
ENGINEFILE=${PWD}/conf/usecases/slidingEventWindowAnalyticsAggregatesAndEMA.yaml
PUBLISHFILE=${PWD}/conf/publishers/fileStandardAnalytics.yaml
```

## Pipeline configuration <a href="#processor-configuration" id="processor-configuration"></a>

This pipeline configures a sliding window of 2.5 second with a 500ms slide which computes aggregates and three window listener functions, all using the same event window.



{% code overflow="wrap" %}
```yaml
processing unit:
  pipeline:
    - filter:
        expression: "symbol == 'CVCO'"
    - time window:
        emitting type: slidingQuoteAnalytics
        aggregate functions:
          SUM: [ volume ]
          MEAN: [ volatility ]
        window listeners:
          - exponential moving average:
              attributes: [ ask, bid ]
              parameters:
                emaFactor: 0.33333
          - minmax normalization:
              attributes: [ ask, bid ]
          - zscore normalization:
              attributes: [ ask, bid ]
        policy:
          type: slidingTime
          slide: 500
          window size: 2500

emit:
  select: "symbol, ask_EMA, bid_EMA, volume_SUM, volatility_MEAN, ask_MINMAX_NORM, bid_MINMAX_NORM, ask_ZSCORE, bid_ZSCORE"

group by:
  - symbol
```
{% endcode %}
