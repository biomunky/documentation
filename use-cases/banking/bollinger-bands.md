# Bollinger bands

This example computes Bollinger bands using a custom-provided function packaged as a jar using the Joule SDK.

## Use case configuration

_File:_ app-bollingerbands-influxdb.env

```bash
SOURCEFILE=${PWD}/conf/sources/stockQuoteStream.yaml
ENGINEFILE=${PWD}/conf/usecases/slidingEventWindowBollingerBandsAnalytics.yaml
PUBLISHFILE=${PWD}/conf/publishers/influxdbBollingerBands.yaml
```

## Pipeline configuration <a href="#processor-configuration" id="processor-configuration"></a>

This pipeline will compute the [Bolinger Bands](https://www.investopedia.com/terms/b/bollingerbands.asp) for the `CVCO` symbol over a sliding window of 5 events.

```yaml
processing unit:
  pipeline:
    - filter:
      expression: "symbol == 'CVCO'"
    - sliding window analytics:
        function: com.fractalworks.streams.examples.banking.analytics.BollingerBands
        window size: 5
        fields: [ ask ]
        parameters:
          deviations: 2

select:
  expression: "symbol, 
               ask_upper_BollingerBand, 
               ask_middle_BollingerBand, 
               ask_lower_BollingerBand"
```

## Function Implementation

This function uses the same implementation pattern as the EMA function.

{% code overflow="wrap" %}
```java
public class BollingerBands implements AnalyticsFunction<Map<String, Double>>  {

    private int deviations = 2;
    public BollingerBands(){}

    @Override
    public Map<String, Double> compute(Number[] values, Number previousValue) {

        Double mean = Arrays.stream(values).mapToDouble(d-> (double) d).sum() / values.length;
        double sqrtmean = 0.0;
        for(int i=0; i<values.length; i++){
            double s =  values[i].doubleValue() - mean;
            sqrtmean += s * s;
        }


        sqrtmean = Math.sqrt( sqrtmean / values.length);
        double band = deviations * sqrtmean;

        Map<String, Double> results = new HashMap<>();
        results.put("upper", mean + band);
        results.put("middle", mean );
        results.put("lower", mean - band);
        return results;
    }

    @Override
    public void setParameters(Properties parameters) {
        if( parameters != null && parameters.containsKey("deviations")) {
            deviations = Integer.parseInt(parameters.get("deviations").toString());
        }
    }

    @Override
    public String getVariablePostFixID() {
        return "BollingerBand";
    }
}
```
{% endcode %}



For Joule to load and initialised the component the analytics class needs to be defined within the `plugins.properties` file under the `META-INF/services` directory

#### Example

```properties
# Change and add lines for your analytics classes
com.fractalworks.examples.banking.analytics.BollingerBands
```

