---
description: >-
  Joule has four user-driven functional blocks which form a use case. This
  sections provides the neccesary documentation to enable develops to build
  Joule projects and extend the system capabilities
---

# Functional components



<table><thead><tr><th width="249">Resource</th><th>Description</th></tr></thead><tbody><tr><td>sources</td><td><p>List of data sources to subscribe to data events and to transform received data to Joule StreamEvent type. </p><p><br>Read <a href="build-your-app-dag/sources.md">sources documentation</a> for further information</p></td></tr><tr><td>use case</td><td><p>A user defined usecase which subscribes to source data streams to perform a series of processes that produce a desired outcome event.</p><p></p><p>Read <a href="build-your-app-dag/use-cases-overview/">use cases documentation</a> for further information</p></td></tr><tr><td>sinks</td><td><p>List of data sinks to publish process events to required data format.</p><p></p><p>Read <a href="build-your-app-dag/sinks.md">sinks documentation</a> for further information</p></td></tr><tr><td>reference data</td><td><p>List of reference data sources that can be used within stream processing pipeline.</p><p></p><p>Read <a href="../reference-data/about-reference-data.md">reference data documentation</a> for further information</p></td></tr></tbody></table>
