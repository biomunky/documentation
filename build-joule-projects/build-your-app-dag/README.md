---
description: >-
  At the heart of the Joule platform is a DAG definition whereby the user
  defines sources, use case, publishers and reference data. This unit forms the
  use case that is deployed in to the system
---

# Build your Joule DAG

<figure><img src="../../.gitbook/assets/Joule DAG.png" alt=""><figcaption><p>Joule Processing DAG</p></figcaption></figure>
