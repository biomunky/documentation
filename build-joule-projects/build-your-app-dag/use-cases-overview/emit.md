---
description: >-
  Events can be optionally published to down stream systems using SQL like
  semantics
---

# Emit

Event output can be refined by using the emit section to limit fields and events to downstream systems, if this is not present events will be published without any filtering.&#x20;

#### Example

```yaml
emit:
  eventType: windowQuoteEvent
  select: "symbol, ask_MIN, ask_MAX, bid_MIN, bid_MAX, BidMovingAverage.avg_bid_max;WHERE symbol=${symbol}"
  having: "symbol !='A'"
```

### DSL Elements

#### eventType

Any events that are published can be retyped by proving a new name.

_Default {existing eventType}-View_

#### select

A SQL project can be defined using a declared comma-separated list of event fields. Metrics and simple math functions are also supported.

Using the below event example event structure we can define SQL like expression&#x20;

```yaml
symbol: IBM
ask: 101
bid: 100
details: {
   vol: 50
   liq: 60   
}
venue: {
   name: nasdaq
}
```

#### _Examples expressions_

<table><thead><tr><th width="169.33333333333331">Format</th><th width="224">Result</th><th>Description</th></tr></thead><tbody><tr><td>ask, bid</td><td>101,100</td><td>Get top level field defined within the event</td></tr><tr><td>ask,bid 'foo'</td><td>101,100</td><td>b will be renamed to 'foo'</td></tr><tr><td>ask,bid,details</td><td>101,100,{vol=50,liq=60}</td><td>All of 'details' variables</td></tr><tr><td>d.vol</td><td>50</td><td>Only the 'details.vol' variable</td></tr><tr><td>details.*</td><td>50,60</td><td>All of 'details' attributes</td></tr><tr><td>ask-bid</td><td>1</td><td>Simple math function executed against variables</td></tr></tbody></table>

#### _Using metrics expressions_

If you are using metrics in the platform these too can be added to the event output. Refer to the [metrics engine documentation](processing-unit.md) on how to set up metrics for your use case.

```yaml
// Metric lookup expression
metric-family.metric1|metric2|metricN;WHERE clause = ${event-field}
```

#### _Example expressions_

<table><thead><tr><th width="261.3333333333333">Format</th><th width="145">Result</th><th>Description</th></tr></thead><tbody><tr><td><p>marketMetrics.avg_ask;where symbol = {symbol}</p><p></p></td><td>101.34</td><td>Query the 'marketMetrics' table where the event symbol joins the matched latest row for attribute 'avg_ask' </td></tr><tr><td><p>marketMetrics.avg_ask|avg_bid;where symbol = {symbol}</p><p></p></td><td>101.34, 100.12</td><td>Query the 'marketMetrics' table where the event symbol joins the matched latest row for attributes 'avg_ask' and 'avg_bid'  </td></tr><tr><td><p>marketMetrics.avg_ask;where symbol = {symbol} AND venue=${venue.name}</p><p></p></td><td>101.24, 100.22</td><td>Query the 'marketMetrics' table where the event symbol and venue joins the matched latest row for the attribute 'avg_ask'</td></tr></tbody></table>

#### having

This is a Javascript expression filter that evaluates the expression against the resulting select project. If the expression passes the resulting event is presented to publishing sinks.

_Optional_

```yaml
// Only publish IBM events
having: "symbol =='IBM'" 
```
