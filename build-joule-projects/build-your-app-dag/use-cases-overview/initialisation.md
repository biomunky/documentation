---
description: >-
  Often advanced use case require reference data support the processing of a
  specific calculation or logic. Joule provides a key feature to load data from
  disk on initialisation
---

# Initialisation

Currently, the initialisation process is limited to importing external data in the in-memory SQL database. This is an optional feature.&#x20;

#### Example

This will load a parquet file in to memory which is accessabile using the provided SQL interface functionality.

```yaml
initialisation:
  sql import:
    schema: banking
    parquet:
      - 
        table: fxrates
        asView: false
        files: [ 'fxrates.parquet' ]
        drop table: true
        index:
          fields: [ 'ccy' ]
          unique: false
```

## SQL Import

Joule has an embedded SQL engine that when enabled various features such as metrics, event capturing, exporting and reference data can be taken advantage of.

Data imported in this manner is typically for static reference data which supports key processing functions within the event stream pipeline.

### Common DLS elements

There are common keywords used in both parquet and CSV importing methods

<table><thead><tr><th width="145">Attribute</th><th width="356">Description</th><th>Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Database schema where tables will be created</td><td>String</td><td>true</td></tr><tr><td>table</td><td>Target table to import data into</td><td>String list</td><td>true</td></tr><tr><td>drop table</td><td><p>Drop existing table before </p><p>import</p></td><td>Drop existing table before import</td><td>false</td></tr><tr><td>index</td><td>Create an index on the created table</td><td>See Below</td><td>false</td></tr></tbody></table>

### Index&#x20;

If this optional field is supplied the index is recreated once the data has been imported.

<table><thead><tr><th width="176">Attribute</th><th width="279">Description</th><th>Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>A list of table fields to base index on.</td><td>String list</td><td>true</td></tr><tr><td>unique</td><td><p>True for a unique index</p><p></p></td><td><p>Boolean</p><p><em>Default true</em></p></td><td>false</td></tr></tbody></table>

### Parquet

Parquet formatted files can be imported into the system. Joule by default will create a view over the passed table. An index cannot be created over a view.

#### Example

```yaml
parquet:
  - 
    table: fxrates
    asView: false
    files: [ 'fxrates.parquet' ]
    drop table: true
    index:
      fields: [ 'ccy' ]
      unique: false
  -
    table: us_holidays
    asView: true
    files: [ 'holidays.parquet' ]
    drop table: true

```

<table><thead><tr><th width="145">Attribute</th><th width="377">Description</th><th width="136">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>asView</td><td>Create a view over the files'. This will mean disk IO for every table query. Consider <code>false</code> if the table is small and accessed frequently. </td><td><p>Boolean</p><p><em>Default true</em></p></td><td>false</td></tr><tr><td>files</td><td>List of files of the same type to be imported</td><td>String list</td><td>true</td></tr></tbody></table>



### CSV

Data can be imported from CSV files using a supported set of delimiters. The key difference between parquet and CSV is you can control the table definition. Joule by default will try to create a target table based upon a sample set of data assuming a header exists on the first row.

#### Example

```yaml
csv:
  - 
    table: nasdaq
    table definition:
      nasdaq (
      ccy VARCHAR,
      name VARCHAR,
      last_sale REAL,
      net_change REAL,
      change REAL,
      market_cap DOUBLE,
      country VARCHAR,
      ipo_year SMALLINT,
      volume INTEGER,
      sector VARCHAR,
      industry VARCHAR);
    file: fxrates.csv
    delimiter: ";"
    date format: "%Y/%m%d"
    timestamp format: "%Y/%m%d"
    sample size: 1024
    skip: 0
    header: true
    auto detect: false
    drop table: true
    index:
      fields: [ 'symbol' ]
      unique: false

```

<table><thead><tr><th width="145">Attribute</th><th width="377">Description</th><th width="136">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>table definition</td><td>Custom SQL table definition used when provided. This will override the <code>auto detect</code> flag.</td><td><em>String</em></td><td>false</td></tr><tr><td>file</td><td>Name and path of the file to import.</td><td>String</td><td>true</td></tr><tr><td>delimiter</td><td>Field delimiter to use. Supported delimiters include <code>, | ; and space and tab</code></td><td><p>String</p><p>Default |</p></td><td>false</td></tr><tr><td>date format</td><td>User specified date format.</td><td><p>String</p><p>Default: System</p></td><td>false</td></tr><tr><td>timestamp formate</td><td>User specified timestamp format.</td><td><p>String</p><p>Default: System</p></td><td>false</td></tr><tr><td>sample size</td><td>Number of rows to use to determine types and table structure.</td><td><p>Integer</p><p>Default: 1024</p></td><td>false</td></tr><tr><td>skip</td><td>Number of rows to skip when auto generating a table using a sample size.</td><td><p>Integer</p><p>Default: 1</p></td><td>false</td></tr><tr><td>header</td><td>Flag to indicate the first line in the file contains a header.</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>auto detect</td><td>Auto detect table format by taking a sample size of data. If set to <code>false</code> a table definition must be provided. </td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>

### _Access_

_To access data loaded using this method Joule provides a SQL API. See further_ [_documentation_](broken-reference) _on how to use this in your custom components_

