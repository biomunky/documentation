---
description: >-
  Joule provides the ability to audit inbound and outbound events to support
  testing, model validation and retraining plus many other uses
---

# Telemetry Auditing

The auditing process systematically records both incoming and outgoing events, assembling them into batches that are written to the in-memory SQL database at predefined intervals. It's important to note that this feature is optional, allowing users to choose whether or not to enable it based on their specific needs.

#### Example

This example will write every inbound and outbound event to the in-memory database every 10 seconds using cloned events for inbound and the resulting event from the final processing step.&#x20;

```yaml
telemetry auditing:
  raw:
    clone events: true
    frequency: 10
  processed:
    clone events: false
    frequency: 10
```

Inbound events are defined as `raw` and outbound as `processed`.

### Core Attributes

Configuration parameters available.

<table><thead><tr><th width="145">Attribute</th><th width="356">Description</th><th width="138">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>clone events</td><td>Flag to inform the platform to perform a deep copy of the event. </td><td>Boolean<br>Default: false</td><td>false</td></tr><tr><td>frequency</td><td>Frequency in seconds to write events to the internal in-memory database. Setting must be greater or equal to 5 seconds</td><td>Integer<br>Default:  5 Secs</td><td>false</td></tr></tbody></table>

