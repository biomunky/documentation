---
description: Stream events are grouped to enable window-based analytics
---

# Group by

Many streaming use cases apply window-based analytics. The GROUP BY clause allows us to apply aggregate functions to groups of events. For example, we might want to display the average bid price for each market symbol. In this case, the aggregate window function reduces the number of events published by the processor.

### group by

List of ordered fields to group by

_Required_ &#x20;

#### Example

```yaml
group by:
  - market
  - symbol
```
