---
description: >-
  Joule is a low-code omni-use case platform designed for contemporary streaming
  analytical processing within hybrid environments and the Internet of Things
---

# Overview

{% hint style="info" %}
Current Joule release v1.1.0
{% endhint %}

Joule is your go-to Low Code Platform for seamless use case development. Simplifying the process, Joule offers an expressive language for defining processing pipelines, utilizing a range of prebuilt and customisable processors, data sources, and sinks.

Straight out of the box, Joule offers standard implementations for data sources and sinks, along with essential core processors, allowing you to kickstart your use case development journey with ease.

## Core Features

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Low Code</strong></mark></td><td>Effortlessly create use cases with Joule DSL, enabling swift development of versatile stream processing pipelines.</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Analytics</strong></mark></td><td>Harness ML model support, auditing, geospatial capabilities, streaming window analytics, SQL metrics engine, and triggers for enhanced functionality.</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Event Transformations</strong></mark></td><td>Enrich, encrypt, and mask data effortlessly with out-of-the-box processors and supported scripting languages (Node.js, JavaScript, Python).</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Data Connectors</strong></mark></td><td>Effortlessly consume and stream consistent data with out-of-the-box connectors, including Kafka, MQTT, data lakes, NoSQL, web sockets, OpenAPI, and more.</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Reference Data</strong></mark></td><td>Utilise the capability to enrich streaming events and calculations with slow-moving data through the embedded distributed cache solution.</td><td></td></tr><tr><td><mark style="color:orange;"><strong>SDK</strong></mark></td><td>An extendable API is available to empower developers in building custom components.</td><td></td></tr></tbody></table>

***

## Platform Architecture

Joules architecture principles is to keep everything as simple as needed to be, provide SDK for customisations and leverage key mature technologies that are able to support a wide array of omni-use cases.

<figure><img src="../.gitbook/assets/Screenshot 2022-11-28 at 21.56.22.png" alt=""><figcaption></figcaption></figure>

Joule out-of-the-box includes standard data connector implementations, core processors, and a DSL, accelerating the development and realisation of use cases.\


***

## Low Code DSL

Joule DSL provides an easy method to build use cases with reusable assets while delivering business impact at pace.

```yaml
pipeline:
  - filter:
      expression: "symbol == 'MSFT'"
  - time window:
      emitting type: tumblingQuoteAnalytics
      aggregate functions:
        MIN: [ask, bid]
        MAX: [ask, bid]
      policy:
        type: tumblingTime
        window size: 5000
```

The DSL is extendable using standard Json annotation when building custom components using the Joule SDK

{% hint style="success" %}
[Learn more](../getting-started/quickstart.md) about getting started in under 15 minutes
{% endhint %}
