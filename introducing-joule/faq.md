---
description: Quickly learn more about Joule
---

# FAQ



<details>

<summary>What would make me use Joule? </summary>

Low-code use case driven process, standard data connector integrations,  OOTB processors, analytics first approach plus more.&#x20;

Spend one hour using the platform we love to gain feedback on whats good and what needs to be improved.

</details>

<details>

<summary>Why would i use Joule verses other solutions such as Flink?</summary>

Joule has been designed to develop and pilot business use cases quickly using a low-code problem solving approach. After proving the use case there is no reason to switch to another platform but we would prefer you stay with Joule.&#x20;

</details>

<details>

<summary>Can i run Joule on bare metal?</summary>

Yes, yes and hell yes. Download one of the example use case project or use the project template to get started.

</details>

<details>

<summary>Can you do Reverse ETL?</summary>

Yes. Joule can present domain data structures using Avro schema when using the Kafka publishing connector.

</details>

<details>

<summary>Is Joule a ETL tool?</summary>

Not really. Joule wasn't designed to be a ETL tool although it does exhibit some of the feature characteristics.&#x20;

</details>
