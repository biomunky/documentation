# MQTT

MQTT is a lightweight, publish-subscribe, machine to machine network protocol for Message queue/Message queuing service. Joule provides the ability to subscribe to events using MQTT source consumer

#### _Driver details_

```groovy
org.eclipse.paho:org.eclipse.paho.mqttv5.client:1.2.5
```

## Configuration Guide

#### Example configuration

```yaml
mqttConsumer:
  broker: tcp://127.0.0.1:1883
  topic: mydevice/leaf

  clientId: myClientId
  username: lyndon
  tenant: uk
  qos: 1

  deserializer:
    transform: com.fractalworks.streams.transport.mqtt.CustomerToStreamEventTranslator
    compressed: false
    batch: false
```

This basic example leverages the default setting resulting in events consumed from the `mydevice/leaf` topic as a Customer object. This object is then transformed in to a Joule StreamEvent object to ready for platform processing.

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of source stream </td><td>String</td><td>true</td></tr><tr><td>broker </td><td>Broker server address</td><td>http://&#x3C;ip-address>:port</td><td>true</td></tr><tr><td>topic</td><td>Message topic to subscribe too</td><td>Strings</td><td>true</td></tr><tr><td>clientId</td><td>A unique client identifier on the server being connected too</td><td>String</td><td>true</td></tr><tr><td>username</td><td>Username</td><td>String</td><td>false</td></tr><tr><td>password</td><td>password</td><td>String</td><td>false</td></tr><tr><td>tenant</td><td>Namespace for created topics</td><td>String</td><td>false</td></tr><tr><td>qos</td><td>Quality of service</td><td><p>Integer</p><p>Default: 0</p></td><td>false</td></tr><tr><td>auto reconnect</td><td>Automatically reconnect to broker on disconnection</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>clean restart</td><td>This means that if a client disconnects and reconnects within 5 minutes with clean start= false,qos>1 then session state data ( e.g subscribed topics, queued messages) are retained</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>sessionExpiry interval</td><td>Maximum time that the broker will maintain the session for once the client disconnects</td><td><p>Long</p><p>Default: 300 (seconds)</p><p>5 minutes</p></td><td>false</td></tr><tr><td>registration message</td><td>Message to send to broker when a Joule process registers</td><td>String</td><td>false</td></tr><tr><td>user properties</td><td>Sets the user properties</td><td>Map&#x3C;String, String></td><td>false</td></tr><tr><td>connection timeout</td><td>This value, measured in seconds, defines the maximum time interval the client will wait for the network connection to the MQTT server to be established.</td><td><p>Integer</p><p>Default: 30 (Seconds)</p></td><td>false</td></tr><tr><td>keepalive interval</td><td>This value, measured in seconds, defines the maximum time interval between messages sent or received. It enables the client to detect if the server is no longer available, without having to wait for the TCP/IP timeout.</td><td><p>Integer</p><p>Default: 30 (Seconds)</p></td><td>false</td></tr><tr><td>deserializer</td><td>Deserialization configuration</td><td>See Deserialization Attributes section</td><td>false</td></tr><tr><td>security</td><td>Security configuration</td><td>See <a href="https://app.gitbook.com/o/GXzsLF1Yj5IM5nJZKy16/s/UU6FZlV07ZD90OzbzGww/~/changes/DdjRirKR0aVmYHFMdxxa/connector-catalog/security">Security documentation</a></td><td>false</td></tr></tbody></table>

### **Deserialization** Attributes

This topic provides configuration parameters available object deserialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>Implementation of StreamEventParser</td><td>true</td></tr><tr><td>compressed</td><td>Uncompress payload using Snappy</td><td><p>Boolean </p><p>Default: false</p></td><td>false</td></tr><tr><td>batch</td><td>Flag to inform process multiple messages have been sent in payload</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Deserializer example**

```yaml
deserializer:
  transform: com.fractalworks.streams.transport.mqtt.CustomerToStreamEventTranslator
  compressed: false
  batch: false
```

#### Additional Resources

* Offical [Mosquitto documentation](https://mosquitto.org/documentation/)
* Good user [documentation](http://www.steves-internet-guide.com)
* MQTT X [UI for testing](https://mqttx.app/docs/downloading-and-installation)
