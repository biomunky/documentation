---
description: >-
  Joule offers RESTful consumer endpoints designed to facilitate seamless
  integration, with a focus on expediting use case development and validation
  processes.
---

# REST

Two types of endpoints are supported

* **File** to bridge batch to stream processing
* **Stream** simplifies cloud integration between Joule processors

#### _Driver details_

```groovy
io.javalin:javalin:5.6.3
```

## File Configuration Guide

The file-based `POST` endpoint excels in efficiently processing large files once they have been fully received. Files are systematically directed to the designated local download directory. Following processing, they are then moved to the local processed directory, distinguished by a completion timestamp.

#### Example file based configuration

```
restFileConsumer:
  name: nasdaq_quotes_file
  topic: quotes
  file format: PARQUET
  download dir: nasdaq/dowloads
```

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="178">Attribute</th><th width="281">Description</th><th width="191">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>User defined topic to be used as the final endpoint component</td><td>String</td><td>true</td></tr><tr><td>file format</td><td>Expected file format to process. Defined as a enumeration, see below for supported file types.</td><td>Enum<br>Default: PARQUET</td><td>false</td></tr><tr><td>download dir</td><td>User defined target directory for  files received</td><td>String<br>Default: See below</td><td>false</td></tr></tbody></table>

#### Supported File Types

* PARQUET
* ARROW\_IPC
* ORC
* CSV
* JSON

## Stream Configuration Guide

The stream-based `POST` endpoint empowers developers to seamlessly chain Joule processes, akin to microservices, thereby facilitating the creation of intricate use cases. For the POST endpoint, a Joule StreamEvent object or array is required.

#### Example stream based configuration

```
restEventConsumer:
  name: nasdaq_quotes_stream
  topic: quotes
```

Events can be posted by using either the 'events' parameter, where specific event-related information is included as part of the request, or through the POST body, where the details of the event are encapsulated within the body of the HTTP POST request.

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="178">Attribute</th><th width="281">Description</th><th width="191">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>User defined topic to be used as the final endpoint component</td><td>String</td><td>true</td></tr></tbody></table>

Note: Currently only the Joule StreamEvent is supported.

## Joule Properties

<table><thead><tr><th width="249">Attribute</th><th width="396">Description</th><th width="221">Data Type</th></tr></thead><tbody><tr><td>joule.rest.downloadPath</td><td>Directory to write files that exceed the in-memory limit. Default: ./downloads</td><td>String</td></tr><tr><td>joule.rest.maxFileSize</td><td><p>The maximum individual file size allowed In GB. </p><p>Default: 2 GB</p></td><td>Integer</td></tr><tr><td>joule.rest.InMemoryFileSize</td><td>The maximum file size to handle in memory in MB. Default: 100 MB</td><td>Integer</td></tr><tr><td>joule.rest.totalRequestSize</td><td>The maximum size of the entire multipart request in GB. Default: 5 GB</td><td>Integer</td></tr></tbody></table>

