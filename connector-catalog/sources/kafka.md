# Kafka

Kafka consumer transport ingests data from subscribed cluster topics. Events consumed are deserialised in to StreamEvents ready for pipeline processing either automatically or through a custom transformer implementation.

#### _Driver details_

```groovy
org.apache.kafka:kafka-clients:2.7.0
```

## Configuration Guide

#### Example configuration

```yaml
kafkaConsumer:
  name: nasdaq_quotes_stream
  cluster address: localhost:9092
  consumerGroupId: nasdaq
  topics:
    - quotes
```

This basic example leverages the default setting resulting in events consumed from the quotes topic as StreamEvent Json object. This assumes the upstream data producer publishes the Joule StreamEvent object as a Json object.

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="197">Attribute</th><th width="217">Description</th><th width="226">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of source stream </td><td>String</td><td>true</td></tr><tr><td>cluster address </td><td>Kafka cluster details. Maps to bootstrap.servers</td><td>http://&#x3C;ip-address>:port</td><td>true</td></tr><tr><td>consumerGroupId</td><td>Consumer group to ensure an event is read only once. Maps to group.id</td><td>String</td><td>true</td></tr><tr><td>topics</td><td>Message topics to subscribe too</td><td>List of strings</td><td>true</td></tr><tr><td>consumerThreads</td><td>Number of consumer threads to use for event parsing</td><td><p>Integer</p><p>Default : 1</p></td><td>false</td></tr><tr><td>pollingTimeout</td><td>The maximum time to block</td><td><p>Long</p><p>Default: 100ms</p></td><td>false</td></tr><tr><td>deserializer</td><td>Deserialization configuration</td><td>See Deserialization Attributes section</td><td>false</td></tr><tr><td>properties</td><td>Additional publisher properties to be applied to the Kafka publisher subsystem</td><td>Properties map</td><td>false</td></tr></tbody></table>

### **Deserialization** Attributes

This topic provides configuration parameters available object deserialization process.&#x20;

<table><thead><tr><th width="224">Attribute</th><th width="220">Description</th><th width="237">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>Implementation of CustomTransformer</td><td>false</td></tr><tr><td>key deserializer</td><td>Domain class that maps to the partition key type. Property maps to key.serializer property</td><td><p>String </p><p>Default: IntegerDeserializer</p></td><td>false</td></tr><tr><td>value deserializer</td><td>Domain class that deserializes incoming object to StreamEvent. Property maps to value.serializer property</td><td><p>String</p><p>Default: StringDeserializer</p></td><td>false</td></tr></tbody></table>

#### **Deserializer example**

```yaml
- kafkaConsumer:
    name: nasdaq_quotes_stream
    cluster address: localhost:9092
    consumerGroupId: nasdaq
    topics:
      - quotes

    deserializer:
      parser: com.fractalworks.examples.banking.data.QuoteToStreamEventParser
      key deserializer: org.apache.kafka.common.serialization.IntegerDeserializer
      value deserializer: com.fractalworks.streams.transport.kafka.serializers.object.ObjectDeserializer
```

#### **OOTB deserializers**

```
com.fractalworks.streams.transport.kafka.serializers.object.ObjectDeserializer
com.fractalworks.streams.transport.kafka.serializers.json.StreamEventJsonDeserializer
com.fractalworks.streams.transport.kafka.serializers.avro.AvroStreamEventDeserializer
```

### Custom Transformers

By implementing a CustomTransformer you can provide domain specific data types to ease integration from upstream applications.

#### **Example**

```yaml
- kafkaConsumer:
    name: nasdaq_quotes_stream
    cluster address: localhost:9092
    consumerGroupId: nasdaq
    topics:
      - quotes

    deserializer:
      parser: com.fractalworks.examples.banking.data.QuoteToStreamEventParser
      key deserializer: org.apache.kafka.common.serialization.IntegerDeserializer
      value deserializer: com.fractalworks.streams.transport.kafka.serializers.object.ObjectDeserializer
```

#### Java transformer implementation&#x20;

This class will convert the passed object to an internal StreamEvent object.

```java
public class QuoteToStreamEventParser implements StreamEventParser {

    public QuoteToStreamEventParser() {
        // Required
    }

    @Override
    public Collection<StreamEvent> translate(Object o) throws TranslationException {
        Collection<StreamEvent> events = null;
        if(o instanceof Quote){
            Quote quote = (Quote) o;
            StreamEvent event = new StreamEvent("quote");
            event.setEventTime(quote.time());
            event.addValue("symbol", quote.symbol());
            event.addValue("bid", quote.bid());
            event.addValue("ask", quote.ask());
            event.addValue("volatility", quote.volatility());
            event.addValue("volume", quote.volume());
            event.addValue("date", quote.date());

            events = Collections.singletonList( event);
        }
        return events;
    }
}
```
