# MinIO S3

MinIO is a High-Performance Object Storage released under GNU Affero General Public License v3.0. It is API compatible with the Amazon S3 cloud storage service. The consumer transport processes bucket objects using notification driven events using batch processing to reduce the memory footprint of the Joule JVM.

#### _Driver details_

```groovy
io.minio:minio:8.5.4
```

#### Example configuration

```yaml
minioConsumer:
  name: "marketdata-S3Consumer"

  connection:
    endpoint: "https://localhost"
    port: 9000
    credentials:
      access key: "XXXXXX"
      secret key: "YYYYYYYYYYYYYYY"

  bucket:
    bucketId: "marketdata"
    object name: "prices"
    versioning: ENABLED
    notifications: ["s3:ObjectCreated:*"]
    retries: 3

  deserializer:
    format: PARQUET
    projection: ["symbol","vol"]

  processed dir: ./processed
  processing rate: 5
  batchSize: 100000
  
```

The above example subscribes to bucket object creation events for a specific file name as a parquet file type. Data will be processed every 5 seconds using StreamEvent batches which are submitted to the processing pipeline. The processed file will be moved to a local `processed` directory on completion of the data ingestion.

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of source stream </td><td>String</td><td>true</td></tr><tr><td>connection </td><td>Connection details</td><td>See <a href="minio-s3.md#connection-attributes">Connection Attributes</a> section</td><td>true</td></tr><tr><td>bucket</td><td>S3 bucket to ingest object data from</td><td>See <a href="minio-s3.md#bucket-attributes">Bucket Attributes</a> section</td><td>true</td></tr><tr><td>deserializer</td><td>Deserialization configuration</td><td>See <a href="minio-s3.md#deserialization-attributes">Deserialization Attributes</a> section</td><td>false</td></tr><tr><td>processed dir</td><td>Directory to place processed files into. If setting is not provided files will be removed from temp store</td><td>String</td><td>false</td></tr><tr><td>processing rate</td><td>Rate of which the consumer will act upon notifications</td><td>Integer<br>Default: 60 Seconds</td><td>false</td></tr><tr><td>batchSize</td><td>Number of records to be read in from file and passed to processing pipeline on a single cycle.</td><td>Long<br>Default:  1024</td><td>false</td></tr></tbody></table>

### **Connection** Attributes

<table><thead><tr><th width="175">Attribute</th><th width="280">Description</th><th width="190">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>endpoint</td><td>S3 service endpoint</td><td>String<br>Default: https://localhost</td><td>false</td></tr><tr><td>port</td><td>Port the S3 service is hosted on</td><td>Integer<br>Default:9000</td><td>false</td></tr><tr><td>url</td><td>Provide a fully qualified url endpoint, i.e. AWS, GCP, Azure urls. This is used over the endpoint setting if provided.</td><td>URL String</td><td>false</td></tr><tr><td>region</td><td>Region where the bucket is to be accessed</td><td>String</td><td>false</td></tr><tr><td>tls</td><td>Use a TLS connection</td><td>Boolean<br>Default: false</td><td>false</td></tr><tr><td>credentials</td><td>IAM access credentials</td><td>See <a href="minio-s3.md#credentials-attributes">Credentials section</a></td><td>false</td></tr></tbody></table>

### Credentials Attributes

For non-production use cases the access/secret keys can be used to prove data ingestion functionality. When migrating to a production environment implement a provider plugin using the provided `JouleProviderPlugin` interface, see basic example below.

<table><thead><tr><th width="178">Attribute</th><th width="281">Description</th><th width="191">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>access key</td><td>IAM user access key</td><td>String</td><td>false</td></tr><tr><td>secret key</td><td>IAM user secret key</td><td>String</td><td>false</td></tr><tr><td>provider plugin</td><td>Custom implementation of credentials ideal for production level deployments</td><td>JouleProviderPlugin implementation</td><td>false</td></tr></tbody></table>

### JouleProviderPlugin Interface

```java
public class JWTCredentialsProvider implements JouleProviderPlugin {
    @Override
    public Provider getProvider() {
        return null;
    }

    @Override
    public void initialize() throws CustomPluginException {
    }

    @Override
    public void validate() throws InvalidSpecificationException {
    }

    @Override
    public void setProperties(Properties properties) {
    }
}
```

### Bucket Attributes

<table><thead><tr><th width="200">Attribute</th><th width="306">Description</th><th width="156">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bucket Id</td><td>Bucket name</td><td>String</td><td>true</td></tr><tr><td>object name</td><td>Object name to listen for events too</td><td>String</td><td>true</td></tr><tr><td>versioning</td><td>Ability to use object versioning. Valid values are either ENABLED or SUSPENDED</td><td>ENUM</td><td>false</td></tr><tr><td>notifications</td><td>Type of bucket notifications to subscribe too. Currently only s3:ObjectCreated:* is supported</td><td>String[]</td><td>false</td></tr></tbody></table>

### **Deserialization** Attributes

This topic provides configuration parameters available object deserialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="306">Description</th><th width="157">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>format</td><td>Object file format. Available formats: PARQUET, ORC, ARROW, CSV</td><td>Enum<br>Default: PARQUET</td><td>false</td></tr><tr><td>projection</td><td>String array of fields to ingest. Consider using this for wide rows otherwise all columns are ingested</td><td><p> String[]</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Deserializer example**

```yaml
deserializer:
   format: PARQUET
   projection: ["symbol","vol"]
```

#### Additional Resources

* Official [MinIO documentation](https://min.io/docs/minio/kubernetes/upstream/)
* MinIO [Docker image](https://hub.docker.com/r/minio/minio)
