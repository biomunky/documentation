---
description: >-
  Data sources provide Joule with the required data to execute use cases.
  Fractalworks has provided a number of ready to use implementations to enable
  you to start building use cases
---

# Sources

***

## Available source implementations

Events consumed are deserialised in to StreamEvents ready for pipeline processing either automatically or through a custom transformer implementation.

## Streaming

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th><th data-hidden></th><th data-hidden></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Kafka</strong></mark></td><td><p>Kafka consumer transport ingests data from subscribed cluster topics. </p><p></p></td><td></td><td></td><td><a href="kafka.md">kafka.md</a></td><td>null</td><td></td><td></td></tr><tr><td><mark style="color:orange;"><strong>Rabbit MQ</strong></mark></td><td>RabbitMQ is lightweight and easy to deploy messaging platform. </td><td></td><td></td><td><a href="rabbitmq.md">rabbitmq.md</a></td><td>null</td><td></td><td></td></tr><tr><td><mark style="color:orange;"><strong>MQTT</strong></mark></td><td><p>Joule provides the ability to subscribe to events using MQTT source consumer</p><p></p></td><td></td><td></td><td><a href="mqtt.md">mqtt.md</a></td><td>null</td><td></td><td></td></tr></tbody></table>

Web

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Rest</strong></mark></td><td>RESTful consumer endpoints designed to facilitate seamless integration</td><td></td><td><a href="rest.md">rest.md</a></td></tr></tbody></table>



## File

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Minio S3</strong></mark></td><td>MinIO file consumer using S3 object level access to cloud or local hosted buckets </td><td></td><td><a href="minio-s3.md">minio-s3.md</a></td></tr><tr><td><mark style="color:orange;"><strong>File Watcher</strong></mark></td><td>Process large event files using stream processing</td><td></td><td><a href="file-watcher.md">file-watcher.md</a></td></tr></tbody></table>
