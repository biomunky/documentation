# RabbitMQ

RabbitMQ is lightweight and easy to deploy messaging platform. It supports multiple messaging protocols. RabbitMQ can be deployed in distributed and federated configurations to meet high-scale, high-availability requirements. Joule provides the ability to consume events using RabbitMQ messaging subscription

#### _Driver details_

```groovy
com.rabbitmq:amqp-client:5.16.0
```

## Configuration Guide

#### Example configuration

<pre class="language-yaml"><code class="lang-yaml"><strong>rabbitConsumer:
</strong>  host: localhost
  exchange:
    name: marketQuotes
    type: TOPIC
  
  routing:
    keys:
      - NASDAQ.IBM
      - NASDAQ.MSFT
      - NASDAQ.GOOG
  
  deserializer:
    transform: com.fractalworks.streams.examples.banking.data.QuoteToStreamEventTransformer
</code></pre>

This example configures a RabbitMQ consumer to receive three event types using the `routing keys` elements from the `topic` exchange type. Quote events are deserialised from a StreamEvent Json object to a Joule StreamEvent object.

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="215">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>host</td><td>RabbitMQ server hostname or Ip address</td><td><p>String</p><p>Default: localhost</p></td><td>false</td></tr><tr><td>port </td><td>RabbitNQ server port, must be greater than 0.</td><td><p>Integer</p><p>Default: 5672</p></td><td>false</td></tr><tr><td>username</td><td>User name</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>password</td><td>password</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>virtualHost</td><td><p>Creates a logical group of connections, exchanges, queues, bindings, user permissions, etc. within an instance.</p><p></p><p>See <a href="https://www.cloudamqp.com/blog/what-is-a-rabbitmq-vhost.html">vhost documentation</a> for further information</p></td><td><p>String</p><p>Default: /</p></td><td>false</td></tr><tr><td>autoAck</td><td>Flag to consider if server should message acknowledged once messages have been delivered. False will provide explicit  message acknowledgment.</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>prefetchCount</td><td>QoS -maximum number of messages that the server will deliver, 0 if unlimited</td><td><p>Integer</p><p>Default: 1</p></td><td>false</td></tr><tr><td>global</td><td>QoS - true if the settings should be applied to the entire channel rather than each consumer</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>durable</td><td>Set a durable queue (queue will survive a server restart)</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>autoDelete</td><td>Server deletes queue when not in use</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>exclusive</td><td>Exclusive queue to this connection</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional queue properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr><tr><td>exchange</td><td>Exchange configuration</td><td>See Exchange Attributes section</td><td>false</td></tr><tr><td>routing</td><td>Routing configuration</td><td>See Routing Attributes section</td><td>false</td></tr><tr><td>deserializer</td><td>Deserialization configuration</td><td>See Deserialization Attributes section</td><td>false</td></tr></tbody></table>

### **Exchange Attributes**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of exchange</td><td>String</td><td>true</td></tr><tr><td>type</td><td><p>In RabbitMQ, messages are published to an exchange and, depending on the type of exchange, the message gets routed to one or more queues. </p><p></p><p> </p></td><td><p>TOPIC, DIRECT, FANOUT, HEADERS</p><p>Default: TOPIC</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional binding properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr></tbody></table>

#### **Exchange example**

```yaml
exchange:
    name: marketQuotes
    type: DIRECT
```

### **Routing Attributes**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>keys</td><td><p>Consumers can selectively consume events sent to a <code>topic</code> exchange by specifying one or more words delimited by dots.</p><p></p><p>Valid key format examples: 'nasdaq.ibm', 'dept.inventory.bolts'.</p><p></p><p>There are two important special cases to define keys.</p><p></p><ul><li>(star) can be substituted for exactly one word</li><li><code>#</code>(hash) can be substituted for zero or more words</li></ul></td><td><p>String</p><p>Default: All</p><p>Constraints: Any number of dot separated word to a limit of 255 bytes</p></td><td>false</td></tr></tbody></table>

#### **Routing example**

This example will subscribe to all events that match the first component of the key `'NASDAQ'`

```yaml
  routing:
    keys:
      - NASDAQ.#

```

### **Deserialization** Attributes

This topic provides configuration parameters available object deserialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>Implementation of StreamEventParser</td><td>true</td></tr><tr><td>compressed</td><td>Not implemented</td><td><p>Boolean </p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Deserializer example**

```yaml
deserializer:
  transform: com.fractalworks.streams.examples.banking.data.QuoteToStreamEventTransformer
  compressed: false
```

## Examples

All examples assume default values when not applied.

### Queue based eventing

Sets up a simple consumer which subscribes to events directly off of a queue.

```yaml
rabbitConsumer:
  queue: point_to_point_queue
  deserializer:
    transform: com.fractalworks.streams.transport.serializers.QuoteToStreamEventTransformer
```

### Work Queues

Subscribe to a _Worker Queue_ that is used to distribute time-consuming tasks among multiple Joule workers.

```yaml
rabbitConsumer:
  queue: worker_queue
  autoAck: false
  durable: true
  prefetchCount: 1

  deserializer:
    transform: com.fractalworks.streams.transport.serializers.QuoteToStreamEventTransformer
```



### Publish/Subscribe

This example each consumer process will receive the same message. Because the exchange is configured as a `fanout` we do not need to use the `routing` element, this will be covered in the next example&#x20;

```yaml
rabbitConsumer:
  exchange:
    name: quotes_exchange
    type: FANOUT

  deserializer:
    transform: com.fractalworks.streams.transport.serializers.QuoteToStreamEventTransformer
```

### Routing

This time we're going to make it possible to subscribe only to a subset of the messages for a single consumer. Therefore each Joule consumer process will have its own `routing keys` they would subscribe to. In the case below a process using this configuration will only receive IBM and MSFT quotes.

<pre class="language-yaml"><code class="lang-yaml"><strong>rabbitConsumer:
</strong>  host: localhost
  exchange:
    name: quotes_exchange
    type: DIRECT

  routing:
    keys:
      - NASDAQ.IBM
      - NASDAQ.MSFT

  deserializer:
    transform: com.fractalworks.streams.transport.serializers.QuoteToStreamEventTransformer
</code></pre>

### Topic

This example is very similar to the previous case whereby we subscribe to events that we are interested in. However, we are using a topic exchange that enabled us to use the dot notation and wildcards to subscribe to a wider set of events. In the case below we would receive all market venue quotes for both IBM and MSFT.&#x20;

<pre class="language-yaml"><code class="lang-yaml"><strong>rabbitConsumer:
</strong>  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    keys:
      - "*.IBM"
      - "*.MSFT"

  deserializer:
    transform: com.fractalworks.streams.transport.serializers.QuoteToStreamEventTransformer
</code></pre>

#### Additional Resources

* Offical [RabbitMQ documentation](https://www.rabbitmq.com/documentation.html)
* CloudAMQP [documentation](https://www.cloudamqp.com/docs/index.html)
