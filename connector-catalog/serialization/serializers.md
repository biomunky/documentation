---
description: >-
  Joule ships with various event formatters and serialisers that convert
  StreamEvent objects to  target streaming or storage formats
---

# Serializers

## Serialization Specification

The `SerializerSpecification` class is used to defined how StreamEvents are serialised for downstream consumption.

### Attributes

<table><thead><tr><th width="140.33333333333331">Attribute</th><th width="389">Description</th><th>Type</th></tr></thead><tbody><tr><td>transformer</td><td>Convert StreamEvent to a custom domain type</td><td>CustomTransformer</td></tr><tr><td>formatter</td><td>Convert a StreamEvent to a target format such as Json, CSV, Object.</td><td><p>Formatter</p><p>Default: Json</p></td></tr><tr><td>compress</td><td>If resulting serialization process needs to compress payload.</td><td><p>Boolean</p><p>Default: False</p></td></tr><tr><td>batch</td><td>Batch events to process</td><td><p>Boolean</p><p>Default: False</p></td></tr><tr><td>properties</td><td>Specific properties required for custom serialization process</td><td>Properties</td></tr></tbody></table>

#### Example

Simple example whereby RabbitMQ publishes StreamEvent as  Json formatted objects. In this example a set of Joule processes would perform inter-process communication to perform a larger use case as a topology.

```yaml
rabbitPublisher:
  exchange:
    name: quotes_exchange
    type: FANOUT

  serializer:
    formatter:
      json formatter: {}
```

## CustomTransformer Interface

This classed is used when the use case requires a specific domain data type, from Joule, for a downstream consumption.  Developers are expected to provide domain specific implementations by implementing the `CustomTransformer` interface.

```java
/**
 * Transform passed byte array to collection of StreamEvents
 *
 * @param payload collection of events to transform to target types
 * @return collection of T
 * @throws TranslationException is thrown when a translation failure occurs
 */
Collection<T> transform(Collection<StreamEvent> payload) throws TranslationException;

/**
 * Transform a single StreamEvent in to a specific type
 *
 * @param payload an event
 * @return
 * @throws TranslationException
 */
T transform(StreamEvent payload) throws TranslationException;
```

#### Example

Class from the nbanking project where StreamEvents are transformed to Quote type.

```java
public class QuoteTransformer implements CustomTransformer<Quote> {

    public QuoteTransformer() {
        // Required
    }

    @Override
    public Collection<Quote> transform(Collection<StreamEvent> payload) throws TranslationException {
        Collection<Quote> quotes = new ArrayList<>();
        if( payload!= null) {
            for(StreamEvent e : payload){
                quotes.add(transform(e));
            }
        }
        return quotes;
    }

    @Override
    public Quote transform(StreamEvent payload) throws TranslationException {
        return new Quote(
                (String)payload.getValue("symbol"),
                (double)payload.getValue("mid"),
                (double)payload.getValue("bid"),
                (double)payload.getValue("ask"),
                (long)payload.getValue("volume"),
                (double)payload.getValue("volatility"),
                (long)payload.getEventTime(),
                (Date)payload.getValue("date")
                );
    }
}
```

### Available Implementations

<table><thead><tr><th width="151.33333333333331">Format</th><th width="322">DSL</th><th>Class</th></tr></thead><tbody><tr><td>avro</td><td>avro serializer</td><td>AvroDomainSerializer</td></tr></tbody></table>

### Avro

This serializer has extra setting to support ability to transport the resulting StreamEvent to the desired the target data domain type using a provided arvo schema. Currently only local schema files are supported with schema registry support on request.

#### Example

```yaml
avro serializer:
    schema: /home/myapp/schema/customer.avsc
```

#### Attributes

<table><thead><tr><th width="200">Attribute</th><th width="269">Description</th><th width="182">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Path and name of schema file</td><td>String</td><td>true</td></tr><tr><td>field mapping</td><td>Custom mapping of source StreamEvent fields to target domain fields</td><td>Map&#x3C;String,String></td><td>false</td></tr></tbody></table>

## Formatters

Formatters are mainly used for direct storage whereby Data tools such as PySpark, Apache Presto, DuckDB, MongDB, Postgres, MySQL etc,.&#x20;

### Available Implementations

<table><thead><tr><th width="151.33333333333331">Format</th><th>DSL</th><th>Class</th></tr></thead><tbody><tr><td>json</td><td>json formatter</td><td>JsonStreamEventFormatter</td></tr><tr><td>csv</td><td>csv formatter</td><td>CSVStreamEventFormatter</td></tr><tr><td>parquet</td><td>parquet formatter</td><td>ParquetStreamEventFormatter</td></tr></tbody></table>

### Json&#x20;

Standard json formater converts processed events to a JSON string using specified attributes

#### Example

```yaml
json formatter:
  date format: YYYY/MM/dd
  contentType: application/json
  indent output: false
```

#### Attributes

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>date format</td><td>Date format to apply to date fields</td><td><p>String</p><p>Default: yyyy/MM/dd</p></td><td>false</td></tr><tr><td>indent output</td><td>Apply indentation formatting</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>contentType</td><td>Type of content to inform receiving application</td><td><p>String </p><p>Default: application/json</p></td><td>false</td></tr><tr><td>encoding</td><td>Payload encoding method</td><td><p>String</p><p>Default: UTF-8</p></td><td>false</td></tr><tr><td>ext</td><td>File extension</td><td><p>String</p><p>Default: json</p></td><td>false</td></tr></tbody></table>

### CSV

Standard csv formatter converts processed events to a CSV string using specified attributes

#### Example

```yaml
csv formatter:
  contentType: text/csv
  encoding: UTF_8
  delimiter: "|"
```

#### Attributes

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>date format</td><td>Date format to apply to date fields</td><td><p>String</p><p>Default: yyyy/MM/dd</p></td><td>false</td></tr><tr><td>delimiter</td><td>Field delimiter</td><td><p>Character</p><p>Default: ","</p></td><td>false</td></tr><tr><td>contentType</td><td>Type of content to inform receiving application</td><td><p>String </p><p>Default: text/csv</p></td><td>false</td></tr><tr><td>encoding</td><td>Payload encoding method</td><td><p>String</p><p>Default: UTF-8</p></td><td>false</td></tr><tr><td>ext</td><td>File extension</td><td><p>String</p><p>Default: csv</p></td><td>false</td></tr></tbody></table>

### Parquet

Converts a StreamEvent to an Avro object using a target schema format before writing to a parquet formatted object.

#### Example

```yaml
parquet formatter:
  schema path: /home/joule/outputschema.avro
  compression codec: SNAPPY
  temp filedir: /tmp
  contentType: binary/octet-stream
  encoding: UTF_8
```

#### Attributes

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema path</td><td>Path location for the Avro output schema </td><td>String</td><td>true</td></tr><tr><td>compression codec</td><td><p>Algorithm to use to compress file. Available types:</p><ul><li>UNCOMPRESSED</li><li>SNAPPY</li><li>GZIP</li><li>LZO</li><li>BROTLI</li><li>LZ4</li><li>ZSTD</li></ul></td><td><p>String</p><p>Default: UNCOMPRESSED</p></td><td>true</td></tr><tr><td>contentType</td><td>Type of content to inform receiving application</td><td><p>String </p><p>Default: binary/octet-stream</p></td><td>false</td></tr><tr><td>encoding</td><td>Payload encoding method</td><td><p>String</p><p>Default: UTF_8</p></td><td>false</td></tr><tr><td>ext</td><td>File extension</td><td><p>String</p><p>Default: parquet</p></td><td>false</td></tr><tr><td>temp file directory</td><td>Directory path for temp files</td><td><p>String</p><p>Default: ./tmp</p></td><td>false</td></tr></tbody></table>



### Supporting classes

To support extendability of the platform the AbstractFormatterSpecification class has been provided.
