---
description: >-
  Incoming and outgoing data require mapping to suitable data formats ready for
  consumption and processing. Fractalworks has provided a number of foundational
  implementations OOTB
---

# Serialization

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th></tr></thead><tbody><tr><td><p><mark style="color:orange;"><strong>Deserialization</strong></mark></p><p></p></td><td>Consumed data is mapped to internal StreamEvents using provided or custom deserialisers</td><td></td><td></td><td></td><td><a href="deserializers.md">deserializers.md</a></td><td>null</td></tr><tr><td><p><mark style="color:orange;"><strong>Serialization</strong></mark></p><p></p></td><td>Data emitted to downstream systems is serialised using provided or custom serialisers</td><td></td><td></td><td></td><td><a href="serializers.md">serializers.md</a></td><td>null</td></tr></tbody></table>
