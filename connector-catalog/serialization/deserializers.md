---
description: >-
  Joule ships with the ability to parse domain types using a custom deserialiser
  along with standard StreamEvent deserialisers
---

# Deserializers

## Deserialization Specification

The `DeserializerSpecification` class is used to defined how StreamEvents are serialised for downstream consumption.

### Attributes

<table><thead><tr><th width="143.33333333333331">Attribute</th><th width="334">Description</th><th>Type</th></tr></thead><tbody><tr><td>parser</td><td>Parser interface to convert a Object to a collection of stream events</td><td><p>StreamEventParser</p><p>Default: StreamEventJsonDeserializer</p></td></tr><tr><td>compressed</td><td>If passed payload is compressed</td><td><p>Boolean</p><p>Default: false</p></td></tr><tr><td>batch</td><td>Is payload a batch os events</td><td><p>Boolean</p><p>Default: false</p></td></tr><tr><td>properties</td><td>Specific properties required for custom deserialization process</td><td>Properties</td></tr></tbody></table>

### StreamEventParser

Parser interface to converts an Object to a collection of stream events.Developers are expected to provide domain specific implementations if there is a need to publish non-Joule events.

```java
/**
* Parse passed byte array to collection of {@link StreamEvent}s
*
* @param payload byte array to translate
* @return collection of stream events
* @throws TranslationException is thrown when a translation failure occurs
*/
Collection<StreamEvent> translate(Object payload) throws TranslationException;
```



### Available Implementations

<table><thead><tr><th width="151.33333333333331">Format</th><th>DSL</th><th>Class</th></tr></thead><tbody><tr><td>json</td><td>json deserializer</td><td>StreamEventJsonDeserializer</td></tr><tr><td>csv</td><td>csv deserializer</td><td>StreamEventCSVDeserializer</td></tr><tr><td>object</td><td>object deserializer</td><td>StreamEventObjectDeserializer</td></tr><tr><td>avro</td><td>avro deserializer</td><td>AvroDomainDeserializer</td></tr></tbody></table>

#### Example

For the provided implementations a full package namespace is required. For the above implementations use `com.fractalworks.streams.sdk.codec`&#x20;

```
deserializer:
   parser: 
      json deserializer: {} 
```

### AVRO

This deserializer has extra setting to support ability to load the target schema. Currently only local schema files are supported with schema registry support on request.

<table><thead><tr><th width="151.33333333333331">Attribute</th><th width="298">Description</th><th width="197">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Path and name of schema file</td><td>String</td><td>true</td></tr><tr><td>field mapping</td><td>Map source feilds to map to a target StreamEvent field</td><td>Map&#x3C;String,String></td><td>false</td></tr></tbody></table>

#### Example

```
deserializer:
   parser: 
      avro deserializer: 
         schema: /home/myapp/schema/customer.avro
```

### CSV

This parser has extra optional setting to support ability to custom parsing through the use of defining a custom header.

<table><thead><tr><th width="151.33333333333331">Attribute</th><th width="298">Description</th><th width="197">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>type map</td><td>Map of fields to types. See supported types</td><td>Map&#x3C;String,Type></td><td>true</td></tr><tr><td>delimiter</td><td>Define a custom file delimiter </td><td>String<br>Default: ","</td><td>false</td></tr><tr><td>date format</td><td>Provide a custom date format. </td><td>String<br>Default: yyyy/MM/dd</td><td>false</td></tr></tbody></table>

#### Example&#x20;

<pre><code><strong>deserializer:
</strong>    parser: 
        csv deserializer:
<strong>            type map:
</strong>               symbol: STRING
               rate:   DOUBLE
</code></pre>

#### Supported Types

Following types are supported

* DOUBLE, FLOAT, LONG, INTEGER, SHORT, BYTE, BOOLEAN, STRING, DATE

### Custom Example

```java
@JsonRootName(value = "quote parser")
public class QuoteToStreamEventParser implements StreamEventParser {

    public QuoteToStreamEventParser() {
        // Required
    }

    @Override
    public Collection<StreamEvent> translate(Object o) throws TranslationException {
        Collection<StreamEvent> events = null;
        if(o instanceof Quote){
            Quote quote = (Quote) o;
            StreamEvent event = new StreamEvent("quote");
            event.setEventTime(quote.time());
            event.addValue("symbol", quote.symbol());
            event.addValue("bid", quote.bid());
            event.addValue("ask", quote.ask());
            event.addValue("volatility", quote.volatility());
            event.addValue("volume", quote.volume());
            event.addValue("date", quote.date());

            events = Collections.singletonList( event);
        }
        return events;
    }
}
```
