# SQL Databases

Publisher transport persists processed events to a configured SQL database and table. The insert statement is dynamicallty generated fom a StreamEvent, attribute names and types need to match the table definition.

This feature is idea for offline analytics, business reporting, dashboards and process testing.

#### _Driver details_

<pre><code><strong>Any compliant JDBC Type 4 driver
</strong></code></pre>

Database specific JDBC driver jar file placed within the userLibs directory.

#### Example configuration

```yaml
sqlPublisher:
  jdbcDriver: org.postgresql.Driver
  host: jdbc:postgresql://localhost:5740
  database: testdb
  table: s1mme
  includeTimestamps: true
  properties:
    ssl: true
    user: postgres
    password: postgres
```

### Core Attributes

Configuration parameters available for the InfluxDB publisher transport. The parameters are organized by order of importance, ranked from high to low.

<table><thead><tr><th width="204">Attribute</th><th width="217">Description</th><th width="246">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>jdbcDriver </td><td>Full jdbc namespace</td><td>String</td><td>true</td></tr><tr><td>host</td><td>JDBC url</td><td>String</td><td>true</td></tr><tr><td>database</td><td>Database name</td><td>String</td><td>true</td></tr><tr><td>table</td><td>Target loading table</td><td>String</td><td>true</td></tr><tr><td>includeTimestamps</td><td>Bucket where event measurements are persisted</td><td><p>Boolean</p><p>Default: False</p></td><td>false</td></tr><tr><td>properties</td><td>Database specific properties. </td><td>Properties map</td><td>true</td></tr><tr><td>batchSize</td><td>Insert batch size </td><td><p>Integer</p><p>Default: 1024</p></td><td>false</td></tr><tr><td>timeout</td><td>Timeout to send queued events to be inserted into database</td><td><p>Long</p><p>Default: 5000ms</p></td><td>false</td></tr><tr><td>autocommit</td><td><p>Sets connection's auto-commit mode to the given state.</p><p></p><p>true to enable auto-commit mode; false to disable it</p></td><td>Boolean<br>Default: false</td><td>false</td></tr></tbody></table>

### Supported databases

All relational databases that support type 4 JDBC driver implementation.
