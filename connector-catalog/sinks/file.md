# File

The file transport provides the ability to stream processed events to a file. This transport is ideal for use case validation, testing, and legacy processing software. Various file formats are supported along with file sizes. &#x20;

### Example configuration

```yaml
file:
  filename: nasdaqAnalytic
  path: /home/app/data/output/analytics
  batchSize: 1024
  timeout: 1000
  formatter:
    csvFormatter:
      contentType: text/csv
      encoding: UTF-8
      delimiter: "|"
```

This basic example leverages the default setting resulting in events published as a CSV file to `/home/app/data/output/analytics/nasdaqAnalytic.csv`

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="150">Attribute</th><th width="173">Description</th><th width="288">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>filename</td><td>file name to use</td><td><p>String</p><p></p></td><td>true</td></tr><tr><td>path</td><td>Absolute path files will be written too</td><td>String</td><td>true</td></tr><tr><td>maxFilesize</td><td>Maximum file size before new file is created</td><td><p>Long</p><p>Default: 67108864 (64MB)</p></td><td>false</td></tr><tr><td>batchSize</td><td>Number of events to trigger a file write process</td><td><p>Integer</p><p>Default: 1024</p></td><td>false</td></tr><tr><td>formatter</td><td>Target format of event data</td><td>See <a href="../serialization/serializers.md">Formatters documentation</a></td><td>true</td></tr></tbody></table>

