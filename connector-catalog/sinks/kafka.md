# Kafka

Kafka publisher transport emits processed events onto a defined cluster topic. Events are serialised using either the default Json StreamEvents serializer or a custom transformer implementation.

#### _Driver details_

```groovy
org.apache.kafka:kafka-clients:2.7.0
```

#### Example configuration

```yaml
kafkaPublisher:
 cluster address: localhost:9092
 topic: customers
 partitionKeys:
    - customerId
```

This simple example leverages the default setting resulting in events published as StreamEvent Json object on to the customers topic using the customer Id as the partitioning key.

An example use case for these settings is when there is a need to chain multiple Joule processors to form a complex, scalable and resilient use case.

### Core Attributes

Configuration parameters available for the InfluxDB publisher transport. The parameters are organized by order of importance, ranked from high to low.

<table><thead><tr><th width="166">Attribute</th><th width="217">Description</th><th width="246">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>cluster address </td><td>InfluxDB server address</td><td>http://&#x3C;ip-address>:port</td><td>true</td></tr><tr><td>topic</td><td>InfluxDB UI / CLI Authentication access token</td><td>String</td><td>true</td></tr><tr><td>partitionKeys</td><td><p>Authentication access details for v1.x InfluxDB. </p><p>Note: Only required if authToken has not been provided.</p></td><td>String</td><td>true</td></tr><tr><td>serializer</td><td>InfluxDB UI / CLI organisation token </td><td>String</td><td>true</td></tr><tr><td>batchSize</td><td>Number of events to batch send, maps to batch.size. Batch size of zero disables batching function</td><td><p>Integer</p><p>Default: 1024</p></td><td>false</td></tr><tr><td>memBufferSize</td><td>Size in bytes of event buffer. Maps to buffer.memory</td><td><p>Long </p><p>Default: 33554432</p></td><td>false</td></tr><tr><td>retries</td><td>Maps to retries</td><td><p>Integer </p><p>Default: 0</p></td><td>false</td></tr><tr><td>properties</td><td>Additional publisher properties to be applied to the Kafka publisher subsystem</td><td>Properties map</td><td>false</td></tr></tbody></table>

### **Serialization** Attributes

This topic provides configuration parameters available for the formatter attribute.&#x20;

<table><thead><tr><th width="161">Attribute</th><th width="220">Description</th><th width="259">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>Implementation of CustomTransformer</td><td>false</td></tr><tr><td>key serializer</td><td>Domain class that maps to the partition key type. Property maps to key.serializer property</td><td><p>String </p><p>Default: IntegerSerializer</p></td><td>false</td></tr><tr><td>value serializer</td><td>Domain class that serializes to Kafka. Property maps to value.serializer property</td><td><p>String</p><p>Default: StreamEventJsonSerializer</p></td><td>false</td></tr></tbody></table>

#### **Serializer example**

```yaml
kafkaPublisher:
 cluster address: localhost:9092
 topic: customers
 partitionKeys:
   - customerId

 serializer:
   key serializer: org.apache.kafka.common.serialization.IntegerSerializer
   value serializer: com.fractalworks.streams.transport.kafka.serializers.json.StreamEventJsonSerializer
```

#### **OOTB serializers**

A flexible event serialization model is provided to enable Joule processes to be chained for complex use cases and provide down stream integrations. Out of the box Joule serializers are provided**,** along with Apache Kafka implementation, while custom serialization is achieved through the Joule SDK.

```
com.fractalworks.streams.transport.kafka.serializers.object.ObjectSerializer
com.fractalworks.streams.transport.kafka.serializers.json.StreamEventJsonSerializer
com.fractalworks.streams.transport.kafka.serializers.avro.AvroStreamEventSerializer
```

### Custom Transformers

By implementing a CustomTransformer you can provide domain specific data types to ease integration to downstream applications.

#### **Example**

```yaml
kafkaPublisher:
 cluster address: localhost:9092
 topic: customers
 partitionKeys:
   - customerId

 serializer:
   transform: com.fractalworks.streams.transport.kafka.CustomerTransformer
   key serializer: org.apache.kafka.common.serialization.IntegerSerializer
   value serializer: com.fractalworks.streams.transport.kafka.serializers.object.ObjectSerializer
```

#### Java transformer implementation&#x20;

This class will convert an internal StreamEvent object to a user defined Customer data type&#x20;

```java
public class CustomerTransformer implements CustomTransformer<Customer> {

   public CustomerTransformer() {
       // Required
   }

   @Override
   public Collection<Customer> transform(Collection<StreamEvent> events) 
   throws TranslationException {

       Collection<Customer> customers = new ArrayList<>();
       for (StreamEvent event : events) {
           customers.add(transform(event));
       }
       return customers;
   }

   @Override
   public Customer transform(StreamEvent event) 
   throws TranslationException {

       Customer customer = new Customer();
       customer.setCustomerId( (int)event.getValue("customerId"));
       customer.setFirstname( (String) event.getValue("firstname"));
       customer.setSurname( (String) event.getValue("surname"));
       return customer;
   }
}
```
