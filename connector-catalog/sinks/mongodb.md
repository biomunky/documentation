# MongoDB

MongoDB publisher transport emits processed events into a defined database collection. Events are serialised using either the provided Document Formatter serializer or a custom transformer implementation.

#### _Driver details_

```groovy
org.mongodb:mongodb-driver-sync:4.8.1
```

#### Example configuration

```yaml
mongodbPublisher:
  servers:
    localhost: 27017
  enable ssl: false

  database: customerdb
  collection: emea_customers
  ordered inserts: true

  credentials:
    user: joule
    password: password
    mechanism: SCRAM_SHA_1

  serializer:
    batch: false
    formatter:
      documentFormatter:
        contentType: application/octet-stream
        encoding: base64
```



### Core Attributes

Configuration parameters available for the MongDB publisher transport.&#x20;

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>database</td><td>Name of database</td><td>String</td><td>true</td></tr><tr><td>collection </td><td>Name of collection inserts will be applied to</td><td>String</td><td>true</td></tr><tr><td>ordered inserts</td><td>Hint to MongDB to insert documents using presented ordering</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>servers</td><td>Map of servers with associated ports</td><td>Map&#x3C;String,Integer></td><td>true</td></tr><tr><td>enable ssl</td><td>Enable SSL</td><td><p>boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>credentials</td><td>Credential configuration</td><td>See Credential Attributes section</td><td>true</td></tr><tr><td>serializer</td><td>Serialization configuration</td><td>See Serialization Attributes section</td><td>false</td></tr></tbody></table>

### Credential Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>username</td><td>Username</td><td>String</td><td>true</td></tr><tr><td>password</td><td>password</td><td>String</td><td>true</td></tr><tr><td>authenication database</td><td>Database to use to authenicate connected user</td><td><p>String</p><p>Default: admin </p></td><td>false</td></tr><tr><td>mechanism</td><td><p>Authenication mechanism</p><p></p><p>Options</p><ul><li>PLAIN</li><li>SCRAM_SHA_1</li><li>SCRAM_SHA_256</li></ul></td><td><p>String</p><p>Default: SCRAM_SHA_1</p></td><td>false</td></tr></tbody></table>



### **Serialization** Attributes

This topic provides configuration parameters available object serialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>CustomTransformer</td><td>false</td></tr><tr><td>formatter</td><td>This is used when a custom transformer is not provided. Useful when chaining Joule processes</td><td><p>Formatter</p><p>Default: documentFormatter (See Document Formatter)</p></td><td>false</td></tr><tr><td>contentType</td><td>Type of content to inform receiving application</td><td><p>String</p><p>Default: application/octet-stream</p></td><td>false</td></tr><tr><td>encoding</td><td>Payload encoding method</td><td><p>String</p><p>Default: base64</p></td><td>false</td></tr><tr><td>batch</td><td>Flag to batch multiple messages in to a single payload</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Serializer example**

The configuration below will serialize StreamEvents as MongoDB documents.

```yaml
serializer:
  batch: false
  formatter:
    documentFormatter:
      contentType: application/octet-stream
      encoding: base64
```

### Document Formatter

Joule ships with a MongoDB document formatter which converts a StreamEvents in to a MongoDB Document type.

### Further details

* Official MongoDB [documentation](https://www.mongodb.com/docs/)
* Official MongoDB [docker image](https://hub.docker.com/\_/mongo)
* Mongo Compass [UI](https://www.mongodb.com/products/compass)&#x20;
