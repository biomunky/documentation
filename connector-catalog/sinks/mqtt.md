# MQTT

MQTT is a lightweight, publish-subscribe, machine to machine network protocol for Message queue/Message queuing service. Joule provides the ability to publish events using MQTT publisher

#### _Driver details_

```groovy
org.eclipse.paho:org.eclipse.paho.mqttv5.client:1.2.5
```

## Configuration Guide

#### Example configuration

```yaml
mqttPublisher:
  clientId: myClientId
  username: lyndon
  tenant: uk
  topic: mydevice/leaf
  qos: 1
  broker: tcp://127.0.0.1:1883

  serializer:
    compress: true

  security:
    jwtclaim:
      audienceId: projectID
      keyFile: file2
      algorithm: RSA
      isPrivateKey: true
```

This basic example leverages the default setting resulting in events published to the `mydevice/leaf` topic as StreamEvent Json object.&#x20;

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of source stream </td><td>String</td><td>true</td></tr><tr><td>broker </td><td>Broker server address</td><td>http://&#x3C;ip-address>:port</td><td>true</td></tr><tr><td>topic</td><td>Message topic to subscribe too</td><td>Strings</td><td>true</td></tr><tr><td>clientId</td><td>A unique client identifier on the server being connected too</td><td>String</td><td>true</td></tr><tr><td>username</td><td>Username</td><td>String</td><td>false</td></tr><tr><td>password</td><td>password</td><td>String</td><td>false</td></tr><tr><td>tenant</td><td>Namespace for created topics</td><td>String</td><td>false</td></tr><tr><td>qos</td><td>Quality of service</td><td><p>Integer</p><p>Default: 0</p></td><td>false</td></tr><tr><td>auto reconnect</td><td>Automatically reconnect to broker on disconnection</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>clean restart</td><td>This means that if a client disconnects and reconnects within 5 minutes with clean start= false,qos>1 then session state data ( e.g subscribed topics, queued messages) are retained</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>sessionExpiry interval</td><td>Maximum time that the broker will maintain the session for once the client disconnects</td><td><p>Long</p><p>Default: 300 (seconds)</p><p>5 minutes</p></td><td>false</td></tr><tr><td>registration message</td><td>Message to send to broker when a Joule process registers</td><td>String</td><td>false</td></tr><tr><td>user properties</td><td>Sets the user properties</td><td>Map&#x3C;String, String></td><td>false</td></tr><tr><td>connection timeout</td><td>This value, measured in seconds, defines the maximum time interval the client will wait for the network connection to the MQTT server to be established.</td><td><p>Integer</p><p>Default: 30 (Seconds)</p></td><td>false</td></tr><tr><td>keepalive interval</td><td>This value, measured in seconds, defines the maximum time interval between messages sent or received. It enables the client to detect if the server is no longer available, without having to wait for the TCP/IP timeout.</td><td><p>Integer</p><p>Default: 30 (Seconds)</p></td><td>false</td></tr><tr><td>serializer</td><td>Serialization configuration</td><td>See Serialization Attributes section</td><td>false</td></tr><tr><td>last will</td><td>Last will specification</td><td>See Last Will Attributes section</td><td>false</td></tr><tr><td>security</td><td>Security configuration</td><td>See <a href="https://app.gitbook.com/o/GXzsLF1Yj5IM5nJZKy16/s/UU6FZlV07ZD90OzbzGww/~/changes/DdjRirKR0aVmYHFMdxxa/connector-catalog/security">Security documentation</a></td><td>false</td></tr></tbody></table>

### **Serialization** Attributes

This topic provides configuration parameters available object serialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation</td><td>CustomTransformer</td><td>false</td></tr><tr><td>formatter</td><td>This is used when a custom transformer is not provided. Useful when chaining Joule processes</td><td><p>Formatter</p><p>Default: jsonFormatter</p></td><td>false</td></tr><tr><td>compressed</td><td>Compress payload using Snappy</td><td><p>Boolean </p><p>Default: false</p></td><td>false</td></tr><tr><td>batch</td><td>Flag to batch multiple messages in to a single payload</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Serializer example**

The configuration below will serialize StreamEvents as compress Json payloads

```yaml
serializer:
  compress: true
  batch: false
```

### **Last Will** Attributes

These attributes provide the configuration parameters to send publisher disconnection notifications to connected subscribers.

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>Topic to sent status message on to</td><td>String</td><td>true</td></tr><tr><td>message</td><td>Message to send to consumers when publisher no longer exists</td><td><p>String </p><p>Default: "Publisher disconnected from broker"</p></td><td>false</td></tr><tr><td>interval delay</td><td>The Server delays publishing the Client's Will Message until the Will Delay Interval has passed or the Session ends, whichever happens first.</td><td><p>Long</p><p>Default: 5 seconds</p></td><td>false</td></tr><tr><td>retain</td><td>If message is retained</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>qos</td><td>Quality of service</td><td><p>Integer</p><p>Default: 1</p></td><td>false</td></tr></tbody></table>

#### **Last will example**

```yaml
last will:
  topic: usecasename/status/failure
  message: "Joule MQTT process disconnected from broker"
  interval delay: 15
```

#### Additional Resources

* Official [Mosquitto documentation](https://mosquitto.org/documentation/)
* Good user [documentation](http://www.steves-internet-guide.com)
* MQTT X [UI for testing](https://mqttx.app/docs/downloading-and-installation)

