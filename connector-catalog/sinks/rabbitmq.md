# RabbitMQ

RabbitMQ is lightweight and easy to deploy messaging platform. It supports multiple messaging protocols. RabbitMQ can be deployed in distributed and federated configurations to meet high-scale, high-availability requirements. Joule provides the ability to publish to RabbitMQ queues, exchanges and topics.

#### _Driver details_

```groovy
com.rabbitmq:amqp-client:5.16.0
```

## Configuration Guide

#### Example configuration

```yaml
rabbitPublisher:
  queue: quotes_queue

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

This will publish StreamEvents as JSON formatted messages onto the `quotes_queue.`

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="215">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>host</td><td>RabbitMQ server hostname or Ip address</td><td><p>String</p><p>Default: localhost</p></td><td>false</td></tr><tr><td>port </td><td>RabbitMQ server port, must be greater than 0.</td><td><p>Integer</p><p>Default: 5672</p></td><td>false</td></tr><tr><td>username</td><td>User name</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>password</td><td>password</td><td><p>String</p><p>Default: guest</p></td><td>false</td></tr><tr><td>virtualHost</td><td><p>Creates a logical group of connections, exchanges, queues, bindings, user permissions, etc. within an instance.</p><p></p><p>See <a href="https://www.cloudamqp.com/blog/what-is-a-rabbitmq-vhost.html">vhost documentation</a> for further information</p></td><td><p>String</p><p>Default: /</p></td><td>false</td></tr><tr><td>autoAck</td><td>Flag to consider if server should message acknowledged once messages have been delivered. False will provide explicit  message acknowledgment.</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>global</td><td>QoS - true if the settings should be applied to the entire channel rather than each consumer</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>durable</td><td>Set a durable queue (queue will survive a server restart)</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>autoDelete</td><td>Server deletes queue when not in use</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>exclusive</td><td>Exclusive queue to this connection</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional queue properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr><tr><td>exchange</td><td>Exchange configuration</td><td>See Exchange Attributes section</td><td>false</td></tr><tr><td>routing</td><td>Routing configuration</td><td>See Routing Attributes section</td><td>false</td></tr><tr><td>serializer</td><td>Serialization configuration</td><td>See Serialization Attributes section</td><td>false</td></tr></tbody></table>

### **Exchange Attributes**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of exchange</td><td>String</td><td>true</td></tr><tr><td>type</td><td><p>In RabbitMQ, messages are published to an exchange and, depending on the type of exchange, the message gets routed to one or more queues. </p><p></p><p> </p></td><td><p>TOPIC, DIRECT, FANOUT, HEADERS</p><p>Default: TOPIC</p></td><td>false</td></tr><tr><td>arguments</td><td>Additional binding properties</td><td><p>Map&#x3C;String, Object></p><p>Default: null</p></td><td>false</td></tr></tbody></table>

#### **Exchange example**

```yaml
exchange:
    name: marketQuotes
    type: TOPIC
```

### **Routing Attributes**

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>event fields</td><td>List of event fields to create dynamic routing keys</td><td><p>String</p><p>Default: None</p></td><td>false</td></tr></tbody></table>

#### **Routing example**

This example will dynamical create route binding using the market and symbol StreamEvent fields. As an example the following would be created `nasdaq.IBM` as a routing key.

```yaml
  routing:
    event fields:
      - market
      - symbol
```

### **Serialization** Attributes

This topic provides configuration parameters available object serialization process.&#x20;

<table><thead><tr><th width="200">Attribute</th><th width="220">Description</th><th width="222">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>transform</td><td>User provided implementation that transforms the StreamEvent in to a domain class. The resulting domain class must implement Serializable interface </td><td>Implementation of StreamEventParser</td><td>false</td></tr><tr><td>formatter</td><td>If a custom transformer has not been provided the for formatter setting will be applied to the processed StreamEvent object.</td><td>Default: jsonFormatter</td><td>false</td></tr><tr><td>compressed</td><td>Not implemented</td><td><p>Boolean </p><p>Default: false</p></td><td>false</td></tr></tbody></table>

#### **Serializer example**

```yaml
serializer:
  formatter:
    jsonFormatter:
      encoding: UTF-8
```

## Examples

All examples assume default values when not applied.

### Queue based eventing

Sets up a simple publisher which sends to Json StreamEvents directly to a named queue.

```yaml
rabbitPublisher:
  queue: quote_queue

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

### Work Queues

Publish to a _Worker Queue_ that is used to distribute time-consuming tasks among multiple Joule workers.

```yaml
rabbitPublisher:
  queue: quote_queue
  messageExpiration: 5000

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

### Publish/Subscribe

This example publishes events to an exchange where consumers receive these events from a connected exchange queue. Because the exchange is configured we do not need to define a queue name.

```yaml
rabbitPublisher:
  exchange:
    name: quotes_exchange
    type: FANOUT

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

### Routing

This time we're going to make it possible to publish events using routing keys so that consumers can selectively subscribe to required events.&#x20;

```yaml
rabbitPublisher:
  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    event fields:
      - market
      - symbol

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

### Topic

This example is very similar to the previous case whereby we publish to events using routing keys. However, since we are using a topic exchange this enables the consuming client to use wildcards to subscribe to a wider set of events, see [RabbitMQ source](../sources/rabbitmq.md) documentation for further information.

```yaml
rabbitPublisher:
  host: localhost
  exchange:
    name: quotes_exchange
    type: TOPIC

  routing:
    event fields:
      - market
      - symbol

  serializer:
    formatter:
      jsonFormatter:
        encoding: UTF-8
```

#### Additional Resources

* Offical [RabbitMQ documentation](https://www.rabbitmq.com/documentation.html)
* CloudAMQP [documentation](https://www.cloudamqp.com/docs/index.html)
