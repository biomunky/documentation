---
description: >-
  Apache Geode provides a database-like consistency model, reliable transaction
  processing and a shared-nothing architecture to maintain very low latency
  performance with high concurrency processing
---

# Geode

Joule uses Geode to as as a high performance event publishing transport whereby events are distributed to a distributed cluster for further processing. This architecture pattern has been production deployed and leveraged within large enterprise solutions over 15 years.

#### _Driver details_

```groovy
org.apache.geode:geode-core:1.15.1
```

## Configuration Guide

#### Example configuration

```yaml
geode publisher:
  connection:
    locator address: localhost
    locator port: 10334

  key: employee_id
  region: employees
  serialization type: PDX
```

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="146">Attribute</th><th width="284">Description</th><th width="213">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>connection</td><td>Cluster connection details</td><td><p>See Connection attributes</p><p></p></td><td>true</td></tr><tr><td>key </td><td>Key attribute</td><td>String<br>Default: uuid</td><td>false</td></tr><tr><td>region</td><td>Storage target region</td><td><p>String</p><p>Default: streamEvents</p></td><td>false</td></tr><tr><td>serialization type</td><td>Serialization method to use. See serialization section.</td><td><p>Enum</p><p>Default: PDX</p></td><td>false</td></tr></tbody></table>

