---
description: For testing purposes a console output processor is provided.
---

# Console

## Configuration Guide

#### Example configuration

```yaml
stdout: {}
```
