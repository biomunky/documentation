---
description: >-
  Data sinks enable Joule to integrate to downstream systems which provide
  further value to additional business cases
---

# Sinks

***

## Available Implementations

### Streaming

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Kafka</strong></mark></td><td><p>Kafka subscriber transport emits events to target topics. </p><p></p></td><td></td><td></td><td></td><td><a href="kafka.md">kafka.md</a></td><td>null</td></tr><tr><td><mark style="color:orange;"><strong>RabbitMQ</strong></mark></td><td><p>RabbitMQ is lightweight and easy to deploy messaging platform. </p><p></p></td><td></td><td></td><td></td><td><a href="rabbitmq.md">rabbitmq.md</a></td><td>null</td></tr><tr><td><mark style="color:orange;"><strong>MQTT</strong></mark></td><td>Joule provides the ability to publish events using MQTT publisher</td><td><p></p><p></p></td><td></td><td></td><td><a href="mqtt.md">mqtt.md</a></td><td>null</td></tr></tbody></table>

### Storage

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-type="rating" data-max="5"></th><th data-hidden></th></tr></thead><tbody><tr><td><p><mark style="color:orange;"><strong>SQL</strong></mark></p><p>Write event micro batches to any databased that supports JDBC Type 4 drivers</p></td><td></td><td></td><td></td><td><a href="sql-databases.md">sql-databases.md</a></td><td>null</td><td></td></tr><tr><td><p><mark style="color:orange;"><strong>InfluxDB</strong></mark></p><p>Standard time series database idea for instrumentation or live KPIs</p></td><td></td><td></td><td></td><td><a href="influxdb.md">influxdb.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>MongoDB</strong></mark></td><td>Publish Json based events to the mature standard document store </td><td></td><td></td><td><a href="mongodb.md">mongodb.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>File</strong></mark></td><td>Write events directly to file using the Joule supported formats</td><td></td><td></td><td><a href="file.md">file.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>MinIO S3</strong></mark></td><td>MinIO file producer using S3 cloud or local hosted buckets</td><td></td><td></td><td><a href="minio-s3.md">minio-s3.md</a></td><td>null</td><td></td></tr><tr><td><mark style="color:orange;"><strong>Geode</strong></mark></td><td>Distributed caching integration that enable advanced use cases</td><td></td><td></td><td><a href="geode.md">geode.md</a></td><td>null</td><td></td></tr></tbody></table>

### Web

<table data-card-size="large" data-view="cards" data-full-width="false"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Web sockets</strong></mark></td><td>Web socket publisher transport emits processed events to web based clients. </td><td></td><td><a href="websocket.md">websocket.md</a></td></tr></tbody></table>
