# Websocket

Websocket publisher transport emits processed events to subscribed clients. Events are serialised as Json using the JsonStreamEventFormatter.

#### _Driver details_

```groovy
io.javalin:javalin:5.6.3
```

#### Example configuration

```yaml
websocketPublisher:
  topic: quotes
```

This simple example leverages the default setting resulting in events published as StreamEvent Json object to connected clients.

### Example Websocket client

Use the below example to validate websocket data streaming. If you are using Chrome open up the Developer tools console tab and click the `Start Streaming` button on the page to stream events from the Joule process.&#x20;

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WebSocket Client</title>
</head>

<body>
    <button onclick="contactServer">Start Streaming</button>
</body>

<script>
    const socket = new WebSocket('ws://localhost:7070/joule/ws/stream/quotes');
    socket.addEventListener('open', function (event) {
        socket.send('Connection Established');
    });

    socket.addEventListener('message', function (event) {
        console.log(event.data);
    });

    const contactServer = () => {
        socket.send("Initialize");
    }
</script>
</html>
```

### Core Attributes

Available configuration parameters

<table><thead><tr><th width="150">Attribute</th><th width="307">Description</th><th width="185">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>topic</td><td>User defined topic this stream relates too</td><td>String</td><td>true</td></tr></tbody></table>

