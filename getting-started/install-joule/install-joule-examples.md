# Install Joule examples

Joule provides a set of examples to demostratre how to use various platform components.The examples can be cloned from the GitLab source repository:

```bash
git clone https://gitlab.com/joule-platform/fractalworks-joule-examples.git
cd fractalworks-joule-examples
gradle clean build
```

The example repository is constantly being extended. So check back often to get updates and new cool stuff.&#x20;
