# Install from source

Joule and almost all of its plugins are open source software. As such, the codebases are freely available to download and build from source. You might install from source if you want the latest code or want to install Joule from a specific commit. This might be helpful when you are contributing changes, or if you want to debug a past change.

To download from source, you would clone the repositories from GitLab, making a local copy, and then run gradle build tasks to install the Joule artifacts.

Downloading and building Joule will enable you to contribute to the project by fixing a bug or implementing a sought-after feature.\


## Installing Joule

To install Joule from the GitLab source repository:

```bash
git clone https://gitlab.com/joule-platform/streamsplatform.git
cd fractalworks-stream-platform
gradle clean build publishToMavenLocal
```

This will install the latest joule jars and dependencies in to the local maven directory.

## Installing connectors

To install Joule connector transports from the GitLab source repository, you will need to have built Joule core first before building these sources.

```bash
git clone https://gitlab.com/joule-platform/fractalworks-stream-transports.git
cd fractalworks-stream-transports
gradle clean build publishToMavenLocal
```

This will install the latest joule transport connectors and dependencies in to the local maven directory.

## Installing Processors

To install Joule from the GitLab source repository, you will need to have built Joule core first before building these sources.

```bash
git clone https://gitlab.com/joule-platform/fractalworks-stream-processors.git
cd fractalworks-stream-processors
gradle clean build publishToMavenLocal
```

This will install the latest joule processors and dependencies in to the local maven directory.



