---
description: Run locally and build custom components
---

# Install Joule

You can install Joule on the command line by using any one of these recommended methods:

* [Uses a Docker image](install-with-docker.md) to install Joule (recommended approach)
* [Install Joule from source](install-from-source.md)

***

## Connectors

Joule works with a number of different data platforms (i.e. databases, No-SQL, and messaging platforms). It does this by providing an implementation for each connection. When you build with Joule you can select which [connectors](broken-reference) to use.

## Processors

Joule ships with a number of event processors (i.e. filters, metrics, window analytics, enrichers etc,.). When you build with Joule you can select which [processors](broken-reference) to use.

