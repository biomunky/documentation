---
description: >-
  Joule enables you to build stream processing pipelines that subscribes to
  source data and publish processed events to destination sinks
---

# Core concepts

***

## Concepts

The following sections describes the concepts you need to know to use Joule.&#x20;

<figure><img src="../.gitbook/assets/concepts.png" alt=""><figcaption></figcaption></figure>

## Event Sources

Data sources provide Joule with the required data to execute use cases. Fractalworks has provided a number of ready to use implementations to enable you to start building use cases.

#### Implementations include

* Kafka
* RabbitMQ
* File Watcher
* Minio S3
* Rest
* MQTT

{% hint style="success" %}
See [**sources**](../connector-catalog/sources/) documentation for further details
{% endhint %}

## Stream Processors

Processors form the core of the Joule platform. A processor performs a distinct unique function when chained with others form a business use case

#### Implementations include

* Enricher
* Transformations&#x20;
* Online prediction
* Event window analytics
* And much more

{% hint style="success" %}
See[ **processors** ](../processors/about-processors/)documentation for further details
{% endhint %}

## Data Sinks

Data sinks enable Joule to integrate to downstream systems which provide further value to additional business cases

#### Implementations include

* Kafka
* Websocket
* SQL Databases
* InfluxDB
* Apache Geode
* And much more

{% hint style="success" %}
See [**sinks**](../connector-catalog/sinks/) documentation for further details
{% endhint %}

## Joule Domain Specific Language

The Joule platform provides a Domain Specific Language (DSL) to define use cases using human-readable yaml syntax.&#x20;

{% hint style="info" %}
See [**Joule DSL**](../build-joule-projects/low-code-dsl.md) documentation for further details
{% endhint %}
