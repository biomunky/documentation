---
description: >-
  Joule is a low-code omni-use case platform designed for contemporary streaming
  analytical processing within hybrid environments and the Internet of Things
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: false
---

# Joule Documentation

***

## Latest Feature Update

<figure><img src=".gitbook/assets/v1.1.0 Release banner.jpg" alt=""><figcaption></figcaption></figure>

## What is Joule

Joule is your go-to Low Code Platform for seamless use case development. Simplifying the process, Joule offers an expressive language for defining processing pipelines, utilizing a range of prebuilt and customisable processors, data sources, and sinks.

Straight out of the box, Joule offers standard implementations for data sources and sinks, along with essential core processors, allowing you to kickstart your use case development journey with ease.

## Get Started

<table data-card-size="large" data-view="cards" data-full-width="false"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>How Joule works</strong></mark></td><td>Learn how to source, process and publish your first Joule process</td><td></td><td></td><td><a href="getting-started/quickstart.md">quickstart.md</a></td></tr></tbody></table>

## Learn the basics

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Sources</strong></mark></td><td>Connect to source data from your live streams, databases and files</td><td></td><td><a href="connector-catalog/sources/">sources</a></td></tr><tr><td><mark style="color:orange;"><strong>Sinks</strong></mark></td><td>Publish computed events to supported tools and platforms</td><td></td><td><a href="connector-catalog/sinks/">sinks</a></td></tr><tr><td><mark style="color:orange;"><strong>Processors</strong></mark></td><td>Use our pre-packaged processors to accelerate your use case</td><td></td><td><a href="processors/about-processors/">about-processors</a></td></tr><tr><td><mark style="color:orange;"><strong>API</strong></mark></td><td>Use existing Joule assets within your own project by using available APIs</td><td></td><td><a href="joule-sdk/api-guides/">api-guides</a></td></tr></tbody></table>

## Do more with your data



<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Analytics</strong></mark></td><td>Build streaming analytics using pre-build analytics and SQL Metrics engine</td><td></td><td><a href="processors/analytics/">analytics</a></td></tr><tr><td><mark style="color:orange;"><strong>Machine Learning</strong></mark></td><td>Predict, score or label streaming events using the PMML processor</td><td></td><td><a href="processors/machine-learning/">machine-learning</a></td></tr><tr><td><mark style="color:orange;"><strong>Transformations</strong></mark></td><td>Apply transformation to events in real-time using pre-build transformers and scripting</td><td></td><td><a href="processors/transformation/">transformation</a></td></tr><tr><td><mark style="color:orange;"><strong>Enrichment</strong></mark></td><td>Add reference data using the embedded low latency  enricher</td><td></td><td><a href="processors/enrichment/">enrichment</a></td></tr><tr><td><mark style="color:orange;"><strong>Filters</strong></mark></td><td>Filter out events by either by event type, field value or using by a user define script</td><td></td><td><a href="processors/filters/">filters</a></td></tr><tr><td><mark style="color:orange;"><strong>Triggers</strong></mark></td><td>Use event delta changes to drive CDC or rules processing</td><td></td><td><a href="processors/triggers/">triggers</a></td></tr><tr><td><mark style="color:orange;"><strong>SDK</strong></mark></td><td>Develop your own data connectors, processors, analytic functions plus much more</td><td></td><td><a href="broken-reference">Broken link</a></td></tr></tbody></table>

