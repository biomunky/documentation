# Declarative Rules

Joule provides a business rules processor that acts upon micro batches within a streaming event context. The Rules processor integrates the JSR-94 compliant Drools engine.&#x20;

#### _Library details_

```groovy
org.drools:drools-XXXX:8.32.0.Final
```

#### Example

```yaml
rules:
  name: anticipated high mobile usage
  rule file: /home/joule/rules/dataUsageRules.drl
```

## Attributes

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name of processor</td><td>String</td><td>false</td></tr><tr><td>rule file</td><td>Drools rule file path</td><td>String</td><td>true</td></tr><tr><td>pool size</td><td>Non-functional setting to size the internal caching of Drools container </td><td><p>Integer</p><p>Default: 8</p></td><td>false</td></tr></tbody></table>



#### Example DRL file

```java
package com.fractalworks.streams.processors.rules;

import com.fractalworks.streams.core.data.streams.StreamEvent;

global com.fractalworks.streams.core.data.streams.Context ctx;
global com.fractalworks.streams.processors.rules.RuleResponse response;

dialect  "mvel"

rule "Low data usage"
    when
        StreamEvent( getValue("dataUsage") < 0.50,
                     getValue("daysleft") >= 15 )
    then
        response.setRuleId("Low data usage");
        response.put("desc","Low data usage");
end

rule "Medium data usage"
    when
        $t : StreamEvent( getValue("dataUsage") > 0.50,
                          getValue("dataUsage") < 0.65,
                          getValue("daysleft") < 10)
    then
        response.setRuleId("Medium data usage");
        response.put("desc","Medium data usage");
end

rule "High data usage"
    when
        $t : StreamEvent( getValue("dataUsage") > 0.90,
                          getValue("daysleft") <= 5)
    then
        response.setRuleId("High data usage");
        response.put("desc","High data usage");
end

```



## Additional Information

Offical Drools documentation can be found [here](https://www.drools.org/learn/documentation.html).
