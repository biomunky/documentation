# Change Data Capture

Change Data Capture (CDC) processor identifies and processes only data that has changed and then makes this changed data available for further use. The process determines change based upon the previous values held for the tracking fields.

#### Example

This will emit changed StreamEvents keyed on symbol when either the broker, bid and ask values change.

```yaml
change data capture:
  key field: symbol
  tracking fields:
    - broker
    - bid
    - ask
  emit only changes: true
  drop duplicates: true
```

## Attributes

<table><thead><tr><th width="214">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>key field</td><td>Key field to </td><td>String</td><td>true</td></tr><tr><td>fields</td><td>List of fields to track and determine changes</td><td>List&#x3C;String></td><td>true</td></tr><tr><td>emit only changes</td><td>A flag to emit only a delta event for the changes seen on the record. If set to false  changes found will be will be added, as a map, to the <code>CDC_FIELD</code> event along with a <code>CDC</code> field being set to true to signify record availability.</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>drop duplicates</td><td>Control flag which can be set to validate processor functionality at runtime. Use a JMX console to set.</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>

