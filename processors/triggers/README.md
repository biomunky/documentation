---
description: >-
  Process triggers minimise expensive computation while informing consumers of
  events to action
---

# Triggers

Real-time alerts and triggers is generally accepted a key feature of stream processing. Joule provides two key features to trigger contextual and rule based change, CDC and declarative rules.&#x20;

## Change Data Capture

Change Data Capture (CDC) processor identifies and processes only data that has changed and then makes this changed data available for further use. The process determines change based upon the previous values held for the tracking fields.

## Declarative Rules

Joule provides a business rules processor that acts upon micro batches within a streaming event context. The Rules processor integrates the JSR-94 compliant Drools engine.&#x20;
