---
description: Transform origin data values in to a masked or independent values
---

# Transformation

***

Customise events in real-time using Joule OOTB transformation processors.&#x20;

### Example use cases

* Mask Personal Identifiable Information (PII)
* Tokenise a data value in to component parts i.e. IMEI to a TAC and device serial Id
* Encrypt sensitive data for endpoint consumption
* Basic bucketing for statitical processing

***

## Available transformation implementations

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Tokenisation</strong></mark></td><td>Tokenise attribute values in to component parts</td><td><a href="field-tokenizer.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Encryption</strong></mark></td><td>Apply RSA encryption techniques along with AES symmetric encryption</td><td><a href="obfuscation/encryption.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Masking</strong></mark></td><td>Mask sensitive field values while retaining the formatting criteria</td><td><a href="obfuscation/masking.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Bucketing</strong></mark></td><td>Apply individual variance tolerances to protect the identity of the originating data </td><td><a href="obfuscation/bucketing.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Redaction</strong></mark></td><td>Eliminate sensitive event field data by either nullifying or replacing with a blank value</td><td><a href="obfuscation/redaction.md"><mark style="color:orange;">Learn more</mark></a></td></tr></tbody></table>
