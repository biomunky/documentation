---
description: >-
  Joule provides an in-stream-obfruscation function to streaming events. This
  function is useful for Personal Identifiable Information de-identification.
---

# Obfuscation

Joule provides an in-stream-obfruscation function to streaming events. This function is useful for Personal Identifiable Information de-identification through the application of encryption, masking or bucketing.

Field level obfuscation is acheived by assigning the required obfuscation type to the field along settable attributes.&#x20;

## Available features

{% tabs %}
{% tab title="Encryption" %}
Encrypt and decrypt event fields using a performant RSA encryption with AES symmetric encryption process. Read [encryption documentation](./#encryption) for more information.



```yaml
obfuscation:
  name: addressEncryption
  fields:
    address:
      encryption:
        decrypt: false
        key location: /tmp
        aes bit length: 128
        rsa bit length: 2048
        rotate keys: true
```
{% endtab %}

{% tab title="Masking" %}
On-the-fly data obfuscation rules are executed on the source data event to enable PII data protection. See [Masking documentation](./#masking) for more information.



```yaml
obfuscation:
  name: numberMasking
  fields:
    creditcard:
      masking:
        pattern: XXXX XXXX XXXX 1234
        mask: "*"
```
{% endtab %}

{% tab title="Bucketing" %}
StreamEvent date and number values can be obfuscated using individual variance tolerances to protect the identity of the orignating data structure. This is more akin to blurring rather than obfuscation.&#x20;



```yaml
obfuscation:
  name: dateBucketing
  fields:
    dateOfBirth:
      date bucketing:
        variance: 30
```
{% endtab %}

{% tab title="Redaction" %}
Effective method of eliminating sensitive event field data by either nullifying or replacing with a blank value.



```yaml
obfuscation:
  name: redactPII
  fields:
    address: 
      redact:
        as null: true
    firstlast_name:
      redact:
        as null: false
```
{% endtab %}
{% endtabs %}

## Obfuscation API

To extend beyond the available obfuscation implementations the below interface is provided for developers to build and deploy new types.

```java
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.specifications.CustomUserType;

/**
 * Base obfuscationType class type
 */
public interface ObfuscationType<T> extends CustomUserType {
    T obfuscate(T value) throws ObfuscationException;

    default void validate() throws InvalidSpecificationException{}
}
```

## Addtional Information

The developer builds and deploys `ObfuscationTypes` in to the Joule platform using the provided SDK.  Follow the instructions on how to [build and deploy](../../../joule-sdk/build-and-deploy.md) custom code using the Joule SDK.&#x20;
