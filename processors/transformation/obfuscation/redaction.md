# Redaction

Effective method of eliminating sensitive event field data by either nullifying or replacing with a blank value.

#### Example

This DSL will replace the contents of the field with blank field.

```yaml
obfuscation:
  name: redactPII
  fields:
    address: 
      redact:
        as null: true
    firstlast_name:
      redact:
        as null: false
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>as null</td><td>If set to true replace value with a null otherwise as a empty string</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>
