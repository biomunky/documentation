# Bucketing

StreamEvent date and number values can be obfuscated using individual variance tolerances to protect the identity of the originating data structure. This is more akin to blurring rather than obfuscation.&#x20;

***

## Date Variance

Each date value for a specified field will be varied by some random number of days, whilst maintaining the original variance, range and distribution. This can useful where it would otherwise be possible to identify individuals by an exact match, such as date of birth. It will for example, give an approximate value for age information.

#### Example

```yaml
obfuscation:
  name: dateBucketing
  fields:
    dateOfBirth:
      date bucketing:
        variance: 30
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>variance</td><td>Maximum number of days to vary the source date</td><td><p>Integer</p><p>Default: 120</p></td><td>false</td></tr></tbody></table>

***

## Number Variance

Each number can be varied by some random percentage, whilst maintaining the original variance, range and distribution. This can useful where it would otherwise be possible to identify individuals  salary by an exact match. It will, for example, give an approximate value for salary information.

#### Example

```yaml
obfuscation:
  name: numberBucketing
  fields:
    salary:
      number bucketing:
        variance: 0.25
    age: 
      number bucketing:
        variance: 0.10
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>variance</td><td>Variance multiplier to be applied to random masking process</td><td><p>Double</p><p>Default: 0.15</p></td><td>false</td></tr></tbody></table>

