# Masking

A technique commonly used by dynamic data masking within the RDBMS. Certain fields are replaced with a mask character (such as an ‘XXX’). This removes the actual content while preserving the same formatting. For example, below masks credit card details while preserving the pattern:

#### Example

```yaml
obfuscation:
  name: numberMasking
  fields:
    creditcard:
      masking:
        pattern: XXXX XXXX XXXX 1234
        mask: "*"
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>mask</td><td>Replacement character to be applied</td><td><p>Char</p><p>Default: *</p></td><td>false</td></tr><tr><td>pattern</td><td>Masking pattern to be applied to source string value</td><td>String</td><td>false</td></tr><tr><td>apply to all</td><td>Boolean to inform if all characters need to be replace using the mask value (true) or to apply pattern (false). </td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>
