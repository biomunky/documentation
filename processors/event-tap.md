---
description: >-
  Tap events directly in to an in-memory database to enable on/off line
  processing
---

# Event Tap

Joule provides the ability to tap an event stream in to an in-process / memory database. This feature is useful to gain data in SQL format for addtional purposes such as off-line analytic. Combining this feature with the provided [Rest API functionality](../joule-sdk/rest-database-endpoints.md) it becomes a powerful feature.

#### Example

```
tap:
  target schema: nasdaq_quotes
  queue capacity: 15000
  flush frequency: 5
  index:
    unique: false
    fields:
      - symbol
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>target schema</td><td>Target table name</td><td>String</td><td>true</td></tr><tr><td>queue capacity</td><td><p>Number of events to queue before flushing to database.</p><p></p><p>Must be greater than 99.</p></td><td><p>Integer</p><p>Default :10000</p></td><td>false</td></tr><tr><td>flush frequency</td><td><p>Frequency the queue is flushed to database table. Either the queue capacity or this attribute triggers queue flush whatever comes first.</p><p></p><p>Must me greater than zereo.</p></td><td><p>Long</p><p>Default: 5 Seconds</p></td><td>false</td></tr><tr><td>alias</td><td>Alias to use instead of the schema name</td><td>String</td><td>false</td></tr><tr><td>is insert</td><td>Update or insert to apply to table changes</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>update criteria fields</td><td>Array of fields used for the update criteria predicate. Only used if <code>is insert</code> set to false</td><td>String[]</td><td>false</td></tr><tr><td>index</td><td>Create a table index</td><td>See Index attributes</td><td>false</td></tr></tbody></table>

### Index Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>uniquie index</td><td>Create a unique index</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>index fields</td><td>Array of fields to create index</td><td>String[]</td><td>true</td></tr></tbody></table>

## SQL Queries

On processor initialisation a set of queries are auto generated which are used to bind Rest API endpoints. The following queries are generated:

### Select

Select data using pagnation either with or without an ingestTime date range. &#x20;

### Export

Ability to export the table either by CSV or Parquet formats.

### Delete

Query to delete rows from the table between an ingestTime date range.&#x20;

