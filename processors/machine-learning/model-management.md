---
description: Deploy a retrained model directly in to a running Joule with zero down time
---

# Model Management

***

## Overview

The predictor processor can be configured to load a mode from a linked reference data store. When this optional setting is applied the relevant logic to switch the model in-place is activated on a model file change occurs.

<figure><img src="../../.gitbook/assets/model-management.png" alt=""><figcaption><p>Example S3 integration for model management</p></figcaption></figure>

### Example using a model store

```yaml
pmml predictor:
  name: irisScorer
  model filename: ./pmml/iris_rf.pmml
  response field: flowerPrediction

  stores:
    modelStore:
      storeName: predictors

  audit configuration:
    target schema: iris_audit
    queue capacity: 1000
    flush frequency: 5
```

### Example model store

The model store implementation uses the reference data store feature. This will bind in the store and listen to file changes to perform the required in-place model swap.

```yaml
reference data:
  name: predictiveModelStores
  data stores:
    - minio stores:
        name: flowerModels
        connection:
          endpoint: "https://localhost"
          port: 9000
          tls: false
          credentials:
            access key: "AKIAIOSFODNN7"
            secret key: "wJalrXUtnFEMIK7MDENGbPxRfiCY"
        stores:
          predictors:
            bucketId: models
```

## Support

Contact [Fractalworks](https://www.fractalworks.io/contact) for details
