---
description: JPMML online predictor processor with auditing and changeable in-place model
---

# Online predictive analytics

***

## Overview

Joule provides a PMML online predictor processor to perform streaming predictions and scoring. The implementation leverages the JPMML open source library developed by [Villu Ruusmann](https://github.com/vruusmann). &#x20;

#### Example

```yaml
pmml predictor:
  name: irisScorer
  model: ./models/iris_rf.pmml
  response field: flowerPrediction
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="299">Description</th><th width="144">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>name of the counter.</td><td>String</td><td>true</td></tr><tr><td>model</td><td>model filename that specifies  path and model filename.</td><td>String</td><td>true</td></tr><tr><td>model store</td><td>Reference data logical store name where the model resides. See Reference Data documentation for further information on configuration.</td><td>String</td><td>false</td></tr><tr><td>features field</td><td>Field where all the required features exist for the model prediction function. This is an optional field where engineered map of features are placed and applied to the model evaluation. See the <a href="feature-engineering/">Feature Engineering</a> documentation for further details.</td><td>String</td><td>false</td></tr><tr><td>response field</td><td>Name of field where the result of the prediction function will be assigned too. </td><td>String</td><td>false</td></tr><tr><td>unpack results</td><td>Unpack the algorithm response variables directly into the StreamEvent object. Otherwise the process will add the complete response object in to the response field.</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>audit configuration</td><td>Every prediction can be audited along with the features used.</td><td>See <a href="model-audit.md">Audit</a>  page </td><td>false</td></tr></tbody></table>

## Support

Contact [Fractalworks](https://www.fractalworks.io/contact) for details
