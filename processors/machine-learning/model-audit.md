---
description: >-
  Audit pre and post model predictions and performance metrics to support model
  observability, explainability and drift management
---

# Model Audit

***

## Overview

This optional configuration provide the ability to audit predications to enable model retraining, feature and prediction drift management, model observability, and any local business governance requirements.&#x20;

The configuration will dynamically create an in-memory database table, using the process name as the target table, and rest endpoints to enable direct access and export functions.

{% hint style="success" %}
Use this feature to support external drift monitoring and model retraining
{% endhint %}

### Example

```yaml
pmml predictor:
  name: irisScorer
  model: ./models/iris_rf.pmml
  response field: flowerPrediction
  
  audit configuration:
    target schema: ml_audit
    queue capacity: 5000
    flush frequency: 300
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="314">Description</th><th width="137">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>target schema</td><td>name of the target schema where the table will be created</td><td>String</td><td>true</td></tr><tr><td>queue capacity</td><td>Number of prediction results to queue before flushing to target table</td><td>Integer<br>Default: 1000</td><td>false</td></tr><tr><td>flush frequency</td><td>Frequency the queue will be flushed to table as seconds</td><td>Long<br>Default: 60 Seconds</td><td>false</td></tr></tbody></table>

## Support

Contact [Fractalworks](https://www.fractalworks.io/contact) for details
