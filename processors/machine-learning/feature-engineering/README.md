---
description: >-
  “Coming up with features is difficult, time-consuming, requires expert
  knowledge. ‘Applied machine learning is basically feature engineering.” —
  Prof. Andrew Ng.
---

# Feature Engineering

***

## Overview

Joule provides a feature engineering processor that enables users to define how features are to be created ready for predictive analytics use cases.

The processor generates for each declared feature field an engineered value. Two methods are supported; raw and compute values using custom expression and plugins.  On completion a feature map is generated will all the required features and placed in the StreamEvent ready for the next processor in the pipeline.

### Example

```yaml
feature engineering:
  name: retailProfilingFeatures
  versioned: true
  features:
    as values:
      - location_code
      - store_id

    compute:
      spend_ratio:
        scripting:
          macro:
            expression: 1 - spend/avg_spend
            language: js
            variables:
              avg_spend: 133.78
      age:
        function:
          age binning:
            source field: date_of_birth
      day:
        function:
          day-of-week transform:
            source field: date
```

### Top level attributes

<table><thead><tr><th width="155">Attribute</th><th width="358">Description</th><th width="133">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Name feature set which is used for a predicting model</td><td>String</td><td>true</td></tr><tr><td>features</td><td>List of supported feature functions</td><td>List</td><td>true</td></tr><tr><td>versioned</td><td>A boolean flag to apply a unique version identifier to the resulting feature map</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr></tbody></table>

### Features Attribute

The features attribute provide two key elements, `as value` and `compute.`&#x20;

### DSL Structure

```yaml
features:
  as values:
    - event field1
    - event field2
    
  compute:
    output_field:
      scripting:
        ...
    other_output_field:
      function:
        plugin_name:
          ... < plugin setting > ...
          event fields:
            - f
          variables:
            varname: value
```

<table><thead><tr><th width="155">Attribute</th><th width="358">Description</th><th width="133">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>as values</td><td>List of event fields whose value will be copied in to the feature map without any changes.</td><td>List</td><td>false</td></tr><tr><td>compute</td><td>List of supported feature functions  mapped to output variables to be executed using the passed event.</td><td>List</td><td>false</td></tr></tbody></table>

_**Note:**_ Either one of the attributes must be defined

***

## Supported Feature Engineering

### As Value

This is most basic function whereby the StreamEvent field value is copied in to the feature map.

#### Example

The below example will copy the defined event field values directly in to the feature map.

```yaml
features:
  as values:
    - location_code
    - store_id
```

### Expression Based

Joule core provides the ability to deploy declariative expressions using the [custom analytics processor](../../analytics/custom-analytics.md). This has been reused within the context of feature engineering to enable users to define custom calculations within the DSL.

#### Example

The below example computes, per event, the spend ration based upon a Javascript expression.&#x20;

```yaml
features:
  compute:
    spend_ratio:
      scripting:
        macro:
          expression: 1 - spend/avg_spend
          variables:
            avg_spend: 133.78
```

***

## Custom Plugins

Developers can extend the feature engineering capabilities by extending the `AbstractFeatureEngineeringFunction` interface. See CustomUserPlugin API documentation for further details.

#### Example

The below example computes, per event, the scale price based upon the MinMax algorithm. This example implements the `AbstractFeatureEngineeringFunction` class.

```yaml
features:
  compute:
    scaled_price:
      function:
        minmax scaler:
          source field: price
          variables:
            min: 10.00
            max: 12.78
```

#### Supported functions

Joule provides a small set of OOTB feature engineering functions.

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td></td><td><a href="scaling.md"><mark style="color:orange;"><strong>Scripting</strong></mark></a></td><td></td></tr><tr><td></td><td><a href="scaling.md"><mark style="color:orange;"><strong>Scaling</strong></mark></a></td><td></td></tr><tr><td></td><td><a href="transform.md"><mark style="color:orange;"><strong>Transform</strong></mark></a></td><td></td></tr></tbody></table>

### Versioning

Every feature map created is versioned using a random UUID. The version is place directly in to the resulting map and accessed using the `feature_version` key.

## Support

Contact [Fractalworks](https://www.fractalworks.io/contact) for details
