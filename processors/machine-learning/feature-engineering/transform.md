---
description: >-
  The process of taking an original value and input variables and derive a
  target feature ready for analytic processing
---

# Transform

## Log Transform

Log transformation is a data transformation method in which it replaces each variable x with a log(x) where x is a positive number and greater than zero

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

#### Example

```yaml
features:
  compute:
    log_spend:
      function:
        log transform:
          source field: spend
```

## Day of Week Transform

Provide the day of week from the passed date object to a number between 1 and 7, where start of week is Monday(1).

#### Supported date objects:

* java.time.LocalDate
* java.sql.Date
* org.joda.time.DateTime

### Attributes

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

#### Example

```yaml
features:
  compute:
    day_of_week:
      function:
        day-of-week transform:
          source field: date
```



## Day Binning

Categorise a day into one of two categories where a weekday (Mon-Fri) is assigned as 1 and 2 for (Sat-Sun)

### Attributes

<table><thead><tr><th width="176">Attribute</th><th width="301">Description</th><th width="130">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

#### Example

```yaml
features:
  compute:
    day_bin:
      function:
        day binning:
          source field: date
```

## Age Binning

Categorise a passed age value, either an integer or date object, in to a pre-configured age bin.

### Attributes

<table><thead><tr><th width="137">Attribute</th><th width="301">Description</th><th width="211">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bins</td><td>Array of age bins to use. Default bins are set to 0-9, 10-19,...110-119.</td><td>Int[][]</td><td>false</td></tr><tr><td>as date</td><td><p>Passed event field is a supported date object.</p><p></p><p>Supported Data classes</p><ul><li>java.time.LocalDate</li><li>java.sql.Date</li></ul></td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>base date</td><td>Provide a date which is used to calculate the age. Default set to the date process is started</td><td><p>String</p><p>Format: YYYY-MM-DD</p></td><td>false</td></tr><tr><td>source field</td><td>The  column to perform the calculation upon</td><td>Double</td><td>true</td></tr></tbody></table>

#### Example

```yaml
features:
  compute:
      age_bin:
        function:
          age binning:
            bins: [ [0,18], [19,21], [22, 40], [41, 55], [56,76]]
            base date: 2023-01-01
            source field: current_age
```
