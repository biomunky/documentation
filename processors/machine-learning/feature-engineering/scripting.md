# Scripting

Joule core provides the ability to deploy declariative expressions using the [custom analytics processor](../../analytics/custom-analytics.md). This has been reused within the context of feature engineering to enable users to define custom calculations within the DSL.

#### Example

The below example computes, per event, the spend ration based upon a Javascript expression.&#x20;

```yaml
features:
  compute:
    spend_ratio:
      scripting:
        macro:
          expression: 1 - spend/avg_spend
          variables:
            avg_spend: 133.78
```

### Attributes

<table><thead><tr><th width="155">Attribute</th><th width="358">Description</th><th width="133">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>expression</td><td>A valid mathematical expression</td><td>String</td><td>true</td></tr><tr><td>language</td><td>List of supported feature functions</td><td><p>String</p><p>Default: js</p></td><td>false</td></tr><tr><td>variables</td><td>Map of variables to be used during the execution of the expression</td><td>Map</td><td>false</td></tr></tbody></table>
