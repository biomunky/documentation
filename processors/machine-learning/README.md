---
description: Leverage streaming online predictions to drive insights to action
---

# Machine Learning

***

For cutting-edge stream-based use cases, the incorporation of machine learning models is integral for next best action processing. Out-of-the-box, Joule offers the capability to seamlessly deploy [JPMML](https://github.com/jpmml) models within a processing pipeline using a predictive processor.

{% hint style="success" %}
Joule provides the capability to perform **near-real-time predictions** to enable best next action use cases
{% endhint %}

### Example use cases enabled by Joule&#x20;

* Online customer segmentation
* Predictive service maintenance
* Behaviour anomaly detection
* Best next action
* Customer conversation scoring

***

## Online stream prediction architecture

The Joule architecture seamlessly integrates core features such as feature engineering, prediction, model auditing, and model management. Below, we showcase a predictive use case, leveraging the robust features that Joule brings to the forefront.

<figure><img src="../../.gitbook/assets/prediction-arch.jpg" alt=""><figcaption><p>Joule Prediction architecture </p></figcaption></figure>

## Available features

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Online predictions</strong></mark></td><td>The prediction processor evaluates event feature vectors in near-real-time </td><td><a href="online-predictive-analytics.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Feature engineering</strong></mark></td><td>Decorate a feature vector with enriched features specific to the deployed model</td><td><a href="feature-engineering/"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Audit</strong></mark></td><td>Monitor and track model performance for prediction explainability and model retraining</td><td><a href="model-audit.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Model Management</strong></mark></td><td>Deployed models are activity managed using an in-place replacement algorithm  </td><td><a href="model-management.md"><mark style="color:orange;">Learn more</mark></a></td></tr></tbody></table>

