# Geospatial

Event based geospatial analytics provide the ability to create location aware solutions. Joule provides a small set of processor optimised for real-time geospatial analytics. Users can define geofences of interest and configure triggers to inform them of occupancy.



<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td></td><td><a href="geofence-occupancy-trigger.md"><mark style="color:orange;"><strong>Occupancy Trigger</strong></mark></a></td><td></td></tr><tr><td></td><td><a href="spatial-index.md"><mark style="color:orange;"><strong>Spatial Index</strong></mark></a></td><td></td></tr><tr><td></td><td><mark style="color:orange;"><strong>Geo Tracker</strong></mark></td><td></td></tr></tbody></table>
