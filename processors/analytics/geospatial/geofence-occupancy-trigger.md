# Geofence Occupancy Trigger



#### Example

```
geofence occupancy trigger:
    name: marketingCampaign
    tracker field: geoTrackingInfo
    plugin: com.fractalworks.streams.examples.telco.marketing.MarketingCampaignMessenger
```



### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Descriptive name of the processor function</td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>tracker field</td><td>Field that holds the <code>GeoTrackingInfo</code> </td><td>String </td><td>true</td></tr><tr><td>plugin</td><td>Custom event plugin that is called for every geofence the event has been </td><td><p>Class: EventType</p><p>See <a href="broken-reference">documentation</a></p></td><td>true</td></tr></tbody></table>

