# Entity Geo Tracker

Stateful based geo tracking using geofencing. State is held per tracking object for entry, dwelling and exit. As events enter, dwell and exit geofence an alert are generated. The event is tracked once it has entered a geofence.

#### Example

```
entity geo tracker:
    name: promotions
    entity key: imsi
    min dwelling time: 2
    timeUnit: MINUTES
    geofences:
      -
        id: 107
        coordinates: ["51.506645","-0.20406286"]
        radius: 150
      -
        id: 106
        coordinates: ["51.503845","-0.2172389"]
        radius: 150
      -
        id: 119
        coordinates: ["51.509743","-0.18553847"]
        radius: 150  
```



## Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>geofences</td><td>List of geofences to trigger entity </td><td>See geofence attribute section</td><td>true</td></tr><tr><td>entity key</td><td>Entity value to track. provide unique key</td><td><p>String</p><p>Default: id</p></td><td>true</td></tr><tr><td>geo tracking info</td><td>StreamEvent key to place tracking information</td><td><p>String</p><p>Default: geoTrackingInfo</p></td><td>false</td></tr><tr><td>latitude field</td><td>StreamEvent field holding latitude value</td><td><p>String</p><p>Default: latitude</p></td><td>false</td></tr><tr><td>longitude field</td><td>StreamEvent field holding longitude value</td><td><p>String</p><p>Default: longitude</p></td><td>false</td></tr><tr><td>dwelling time</td><td>Minimum dwelling time within a geofence</td><td><p>Long</p><p>Default: 15 Seconds</p></td><td>false</td></tr><tr><td>timeUnit</td><td><p>Time unit used to assess dwelling time.</p><p>Supported units:</p><ul><li>SECONDS</li><li>MINUTES</li><li>HOURS</li></ul></td><td><p>TimeUnit</p><p>Default: SECONDS</p></td><td>false</td></tr><tr><td>default radius</td><td>Geofence radius override to be applied if reference data  is missing</td><td><p>Float</p><p>Default: 4.0f ( xx feet)</p></td><td>false</td></tr><tr><td>spatial index</td><td>An area is set out as a search tree. The default area is a flattened world divided out in to rectangles.</td><td>See <a href="spatial-index.md">spatial index documentation</a></td><td>false</td></tr></tbody></table>

## Geofence Attribute

A geofence is a circle defined up a unique Id, its centre point(latitude and longitude coordinates)  and radius. One or more geofences are defined as a list.

```
geofences:
  -
    id: 1000
    coordinates: ["51.4623328","-0.1759467"]
    radius: 150
  -
    id: 2000
    coordinates: ["51.5136287","-0.1137969"]
    radius: 150
```

### Attributes

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>id</td><td>Unique Id for the geofence</td><td>Integer</td><td>true</td></tr><tr><td>coordinates</td><td>Centre latitude and longitute coordinates defined as string to ensure correct resolution</td><td>String Array</td><td>true</td></tr><tr><td>radius</td><td>Radius of geofence in feet</td><td>Float</td><td>true</td></tr></tbody></table>

### Geo Tracking Info Field

The geoTrackingInfo field, or the one specified by the developer, is added to the processed StreamEvent object on processing completion. A `GeoTrackingInfo` object is assigned to the field. Below are the attributes held within the object.&#x20;

```markup
Key: geoTrackingInfo 
Value: GeoTrackingInfo
      - trackingTag
      - currentSpeedAndDirection
      - previousSpeedAndDirection
      - distanceTraveled
      - previousDistanceTraveled
      - previousLat
      - previousLng
      - currentLat
      - currentLng
      - geoFenceOccupancyState
      - previousUpdatedTimestamp
      - currentUpdatedTimestamp
```
