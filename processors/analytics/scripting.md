# Scripting

Joule provides a flexible and extendable model of component development which includes executing external scripts or defined expression within the use case DSL.&#x20;

Currently Javascript, Python and node.js are supported with additional languages being added with respect to roadmap commitments.&#x20;

#### Example

The below Javascript expression calculations the total compensation using StreamEvent fields which are passed in to the processing context.

```yaml
scripting:
    expression: salary + bonus + pensionContribution
    expand all: true
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>expression</td><td>Expression to evaluate against the passed event</td><td>String</td><td>false</td></tr><tr><td>script</td><td>Name and location of script to be used</td><td>String</td><td>false</td></tr><tr><td>method</td><td>Method to call within script</td><td><p>String</p><p>Default: processEvent</p></td><td>false</td></tr><tr><td>response attribute</td><td><p>Name of attribute to be used when adding the result in to the StreamEvent object.</p><p></p><p>If the processor is set as stateful this value is also added to the processing context.</p></td><td><p>String</p><p>Default: result</p></td><td>false</td></tr><tr><td>response datatype</td><td><p>Response data type returned from either the expressoin or script method.</p><p></p><p>Supported data types: DOUBLE, FLOAT, LONG, INTEGER, SHORT, BYTE, BOOLEAN, STRING</p></td><td><p>String</p><p>Default: DOUBLE</p></td><td>false</td></tr><tr><td>expand all</td><td>Expand all StreamEvent attributes within the scripting process context to enable access</td><td><p>Boolean</p><p>Default:</p></td><td>false</td></tr><tr><td>stateful</td><td>Cache and reuse the response attribute for further processing.</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr></tbody></table>

## External scripts

The processor can also load external javascript script which may have complex set of functions that can be leveraged.&#x20;

#### Example

The below DSL definition will load `totalCompensationCalc.js` file in to the scripting engine, execute the `totalCompensationCalc` with the result as `totalCompensation` being added to the the StreamEvent object.

```yaml
scripting:
    method: totalCompensationCalc
    response attribute: totalCompensation
    script: /var/joule/scripts/totalCompensationCalc.js
```

#### Example Javascript file

Simple example which demostrates the key capability.

```javascript
function totalCompensationCalc(){
    return salary + bonus + pensionContribution;
}
```

## Language Support

The following languages are supported using external file execution. More languages to be added upon feature requests.

* Python
* Javascript
* node.js
