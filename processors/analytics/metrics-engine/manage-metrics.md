# Manage Metrics





## Runtime Policy

This section configures when to start and how frequently metrics are cacluated. Below the metrics computation will start after 2 minutes on process startup and thereafter every 1 minute.

#### Example

```yaml
truncate on start: true
compaction policy:
  frequency: 8
  time unit: HOURS
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>frequency</td><td>Frequency metrics are computed. Minimum 15 second computation cycles</td><td><p>Long</p><p>Default: 1</p></td><td>true</td></tr><tr><td>startup delay</td><td>First compute cycle delay.</td><td><p>Long</p><p>Default: 5</p></td><td>true</td></tr><tr><td>time unit</td><td><p>Time unit used to set the scheduled processing policy</p><p></p><p>Supported units: SECONDS, MINUTES, HOURS</p></td><td><p>TimeUnit</p><p>Default: MINUTES</p></td><td>false</td></tr></tbody></table>



***

### Metric Management Attributes

Each metric defintion includes management attributes to control how the metric table is managed throughout the Joule process lifetime. Since Joule uses an in-memory database the size of table needs to be managed

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>truncate on start</td><td>Truncate the mtric tables on startup</td><td><p>Boolean</p><p>Default: true</p></td><td>true</td></tr><tr><td>compaction policy</td><td></td><td></td><td>true</td></tr><tr><td>           frequency</td><td>Frequency table compactions will occur</td><td><p>long</p><p>Default: 1</p></td><td>true</td></tr><tr><td>           time unit</td><td><p>Time unit used to set the scheduled companion policy</p><p></p><p>Supported units: MINUTES and HOURS</p></td><td><p>TimeUnit</p><p>Default: HOURS</p></td><td>true</td></tr></tbody></table>



