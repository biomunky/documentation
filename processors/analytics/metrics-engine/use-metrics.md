# Use Metrics

## Metrics Access

Metrics can be presented on the final output stage within the `emit select` statement using a well formed metric query structure.

### Declaratively

Users defined a metric query within the select statement with the results being added to the output event. The below example performs a metric lookup based upon the passed `symbol` value.&#x20;

#### Example

```xml
emit:
  select: "symbol, BidMovingAverage.avg_bid_max;WHERE symbol=${symbol} 'avg_bid_max'"
```

### Query Structure

The declarative query structure is formed of three required components and an optional override output alias.

```yaml
<metric-family>.<metrics>;<predicate> '<output-alias>'
```

#### metric-family

This is the logical name of the group of metrics, which should correspond to a metric name declared within the compute section.

#### metrics

A comma delimited list of metrics or asterisk. Any defined metric needs to correspond the query projection attributes.&#x20;

#### predicate

Matching and filtering statement using standard SQL. This is used to extract only those records that fulfill a specified condition.

<pre><code><strong>-- Simple lookup using a 
</strong>WHERE symbol='IBM'

-- Parameter replace using the corresponding the event object symbol value
WHERE symbol=${symbol}

-- Apply additonal filtering criteria
WHERE symbol=${symbol} AND avg_bid_max > 120.88
</code></pre>

#### Examples

```
-- Get all metrics from the BidMovingAverage metrics family for IBM 
BidMovingAverage.*;WHERE symbol='IBM'

-- Get the avg_bid_max metric from the BidMovingAverage metrics family all symbols matching the reference ${symbol} variable 
BidMovingAverage.avg_bid_max;WHERE symbol=${symbol}

-- Same as above with the ability to define a custom name for the resulting field
BidMovingAverage.avg_bid_max;WHERE symbol=${symbol} 'avg_bid_max'

-- Get the avg_bid_max and avg_bid_min metric from the BidMovingAverage metrics family
BidMovingAverage.avg_bid_max,avg_bid_min;WHERE symbol=${symbol}
```

## Query Interface

Metrics can be used within custom processors my using the Metric Query API, see [API documentation](../../../joule-sdk/api-guides/analytics-api/create-custom-metrics.md) for details on how to leverage this capability.
