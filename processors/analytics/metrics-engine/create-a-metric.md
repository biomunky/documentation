---
description: Define a set of metrics with streamed events
---

# Create a metric

***



## Compute

This section configures a list of metrics to be computed. One or metric families can be configured. Each metric configuration has there own management policies&#x20;

```yaml
foreach metric compute:
  metrics:
    - name: BidMovingAverage
      metric key: symbol
      table definition: standardQuoteAnalyticsStream.BidMovingAverage (symbol VARCHAR, avg_bid_min FLOAT, avg_bid_avg FLOAT,avg_bid_max FLOAT)
      query:
        SELECT symbol,
          MIN(bid) AS 'avg_bid_min',
          AVG(bid) AS 'avg_bid_avg',
          MAX(bid) AS 'avg_bid_max'
        FROM standardQuoteAnalyticsStream.quote
        WHERE
          ingestTime >= date_trunc('minutes',now() - INTERVAL 2 MINUTES) AND ingestTime <= date_trunc('minutes',now())
        GROUP BY symbol
        ORDER BY 1;
      truncate on start: true
      compaction policy:
        frequency: 8
        time unit: HOURS
```



### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Unique metric name</td><td>String</td><td>true</td></tr><tr><td>metric key</td><td>Key for the metric</td><td>String</td><td>true</td></tr><tr><td>query</td><td>Metrics query</td><td>SQL query</td><td>true</td></tr><tr><td>table definition</td><td>SQL table for the resulting metrics</td><td>SQL table definition</td><td>true</td></tr></tbody></table>
