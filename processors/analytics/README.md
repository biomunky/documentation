---
description: >-
  Analytics form the core of the platform to drive insight to value. Joule
  provides various methods to define and leverage analytics within a streaming
  context
---

# Analytics

***

Joule's **Analytics** features provide you to the ability to define custom calculations, facilitating the implementation of specific use cases on your stream events, like:

* Scheduled metrics
* Window analytics
* Scripting analytics

{% hint style="info" %}
Joule's Expression based feature lets you define custom calculation either using Javascript, node.js or Python which will get evaluated on the fly per event. It is especially useful for the data and development teams that generally deal with Python or Javascript
{% endhint %}

* Geospatial&#x20;
* HyperLogLog

## In this section

See the following guides to learn more about the different analytics features and their usage:

<table><thead><tr><th width="180">Guide</th><th>Description</th></tr></thead><tbody><tr><td><a href="metrics-engine/">Metrics engine</a></td><td>A SQL compliant metrics engine which computes metrics using a runtime policy using the events stored</td></tr><tr><td><a href="window-analytics/">Window Analytics</a></td><td>Standard window functions are provided to perform time and count based analytics</td></tr><tr><td><a href="scripting.md">Scripting</a></td><td>Scripting based model to define analytics</td></tr><tr><td><a href="geospatial/">Geospatial</a></td><td>Optimised real-time geospatial analytics for geofence occupancy and tracking</td></tr><tr><td><a href="hyperloglog.md">HyperLogLog</a></td><td>Probabilistic counter for large or high cardinality datasets</td></tr></tbody></table>

***

**Questions**, contact us by [email](https://app.gitbook.com/u/pdONHRVvqtdGUlip9TBy5lLPNp02)
