# Tumbling

Tumbling windows are a non-overlapping events arrays ordered by time. The size of the window is dependent upon policy configuration, either time or event count. Events within the window are only processed once when a new window is triggered.

## Time based attributes

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>window size</td><td>Size of window with respect to timeUnit</td><td>Long</td><td>true</td></tr><tr><td>time unit</td><td><p>TimeUnit used for configuring window trigger. Supported types:</p><ul><li>NANOSECONDS</li><li>MICROSECONDS</li><li>MILLISECONDS</li><li>SECONDS</li><li>MINUTES</li><li>HOURS</li><li>DAYS</li></ul></td><td><p>TimeUnit</p><p>Default: MILLISECONDS</p></td><td>false</td></tr></tbody></table>

#### Example

Configures a tumbling time based window with a size of 10 seconds.&#x20;

```
time window:
  emitting type: pageFailures
  policy:
    type: tumblingTime
    window size: 10
    time unit: SECONDS
```

## Count based attributes

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>window size</td><td>Maximum number of events before triggering window processing</td><td>Long</td><td>true</td></tr><tr><td>delay</td><td><p>Time period to trigger  window processing if window size has not been breached.</p><p>Note: If delay is set to zero this addition trigger is not activated.</p></td><td><p>Long</p><p>Default: 0</p></td><td>true</td></tr><tr><td>time unit</td><td><p>TimeUnit used for configuring window delay. Supported types:</p><ul><li>NANOSECONDS</li><li>MICROSECONDS</li><li>MILLISECONDS</li><li>SECONDS</li><li>MINUTES</li><li>HOURS</li><li>DAYS</li></ul></td><td><p>TimeUnit</p><p>Default: MILLISECONDS</p></td><td>false</td></tr></tbody></table>

#### Example

Configures a tumbling window with a maximum of 1000 events and an addtional window trigger set to 5 seconds

```
time window:
  emitting type: pageFailures
  policy:
    type: tumblingCount
    window size: 1000
    delay: 5
    time unit: SECONDS
```
