# Event Windows

Joule provides the ability to perform time or number of events window based analytics. A suite of standard window functions are provided out-of-the-box. Developers can create custom analytic functions which are deployable as discrete components on the classpath.



{% tabs %}
{% tab title="Tumbling" %}
Tumbling windows are a non-overlapping events arrays ordered by time. In a tumbling window, events are grouped in to a single window based on time or count. An event belongs to only one window. Events within a window are only processed once when a new window is presented for processing.

<figure><img src="../../../.gitbook/assets/tumbling.png" alt=""><figcaption></figcaption></figure>
{% endtab %}

{% tab title="Sliding" %}
Sliding windows, unlike Tumbling windows, output events only for points in time when the content of the window actually changes. In other words, when an event enters or exits the window. So, every window has at least one event. Note events can belong to more than one sliding window.

<figure><img src="../../../.gitbook/assets/slide.png" alt=""><figcaption></figcaption></figure>
{% endtab %}
{% endtabs %}

### Example

```yaml
processing unit:
  pipeline:
    - time window:
        emitting type: tumblingQuoteAnalytics
        aggregate functions:
          FIRST: [ask]
          LAST: [ ask ]
          MIN: [ ask ]
          MAX: [ bid ]
          SUM: [ volume ]
          MEAN: [ volatility ]
          VARIANCE: [ volatility ]
          STDEV: [ volatility ]
        policy:
          type: tumblingTime
          window size: 5000
```

### Core Attributes

<table><thead><tr><th width="209">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>emitting type</td><td>name of the event</td><td>String</td><td>true</td></tr><tr><td>preprocessor</td><td>Window preprocessor</td><td></td><td>false</td></tr><tr><td>window listeners</td><td>Custom window listeners which are executed on window generation. See window listener documentation below</td><td>See documentation below.</td><td>false</td></tr><tr><td>aggregate functions</td><td>Standard set of aggragation functions based over a window using group by semantics</td><td>See documentation below.</td><td>false</td></tr><tr><td>policy</td><td><p>Type of window processing. Supported types: </p><ul><li><p>Tumbling</p><ul><li>tumblingTime</li><li>tumblingCount</li></ul></li><li><p>Sliding</p><ul><li>slidingTime</li><li>slidingCount</li></ul></li></ul></td><td><p>String</p><p>e.g. slidingCount</p></td><td>true</td></tr></tbody></table>

## Preprocessor

Before a window is process the user is able to inject a preprocessing function. The function returns a new window which is subsequently processed by defined window listeners and aggregate functions.

```java
import com.fractalworks.streams.core.data.streams.OrderedStreamEventWindow;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.data.streams.StreamTimeType;
import com.fractalworks.streams.core.data.streams.Window;
import com.fractalworks.streams.sdk.analytics.WindowPreprocessor;

/**
* Custom preprocessor 
*/
public class MyCustomPreprocessor extends WindowPreprocessor {

    public MyCustomPreprocessor(StreamTimeType streamTimeType, int count, int groupByKey) {
        super(streamTimeType, count, groupByKey);
    }

    public Window compute(Window window) {
        StreamEvent[] orderedEvents = // Do something interesting
        return new OrderedStreamEventWindow(orderedEvents, groupbyKey, streamTimeType);
    }
}

```

## WindowListener API

Custom window listeners can be developed and deployed in to the Joule environment. See [building and deploying custom components](../../../joule-sdk/build-and-deploy.md) documentation for further information.

### API

```java
package com.fractalworks.streams.core.listeners;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Window;

import java.util.Map;

public interface WindowListener {
    Tuple<String, Map<String, Object>> apply(Window window, Context context);
}
```

## Aggregation Functions

Joule provides a set of standard aggregate functions that can be applied to a set of events within a triggered window.&#x20;

### Available functions

When the function applied the field the result is added in the returned stream events as an additional field e.g `<field_name>_SUM`

<table><thead><tr><th width="272">Type</th><th>Description</th></tr></thead><tbody><tr><td>SUM</td><td>Sum of field values</td></tr><tr><td>MIN</td><td>Min of field value</td></tr><tr><td>MAX</td><td>Max of field value</td></tr><tr><td>MEAN</td><td>Mean of field value</td></tr><tr><td>VARIANCE</td><td>Variance of field value</td></tr><tr><td>STDEV</td><td>Standard deviation of field value</td></tr><tr><td>FIRST</td><td>First field value in window</td></tr><tr><td>LAST</td><td>Last field value in window</td></tr><tr><td>HARMONIC_MEAN</td><td>Harmonic mean of field value</td></tr><tr><td>GEOMETRIC_MEAN</td><td>Geometric mean of field value</td></tr><tr><td>PVARIANCE</td><td>Population variance of field value</td></tr><tr><td>SECOND_MOMENT</td><td>Seond moment of field value</td></tr><tr><td>SUM_SQRTS</td><td>Sum of square root of field value</td></tr><tr><td>SUM_LOGS</td><td>Sum of log of field value</td></tr></tbody></table>
