# Custom Analytics

Platform users can deploy custom stateful and stateless analytics using the Joule DSL via the analytics processor as expressions. Additionally  developers can extend the analytical capabilities by implementing various analytics APIs.&#x20;



