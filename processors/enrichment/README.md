---
description: Apply fresh reference data using low latency data store to streaming events
---

# Enrichment

***

In sophisticated streaming scenarios, the execution of advanced use cases frequently necessitates supplemental external data for driving processing. The enrichment solution pattern is typically deployed, leveraging low-latency data stores to ensure swift and responsive data retrieval.

## Low latency reference data architecture

OOTB Joule has provided the heavy lifting required to deploy a low-latency solution so that developers can focus on building advanced use case that required addition data.&#x20;

#### Example use cases&#x20;

* Customer data bundle plan, credit rating etc;
* Company market rating, previous day stock sensitives etc;
* Cell tower signal coverage, machine utilisation scaling limits etc;
* Static Geofences for places of interest
* Dynamic business trigger parameters (i.e. SLAs, stock price levels...)
* General domain level static KPI / metrics

<figure><img src="../../.gitbook/assets/reference data arch.png" alt=""><figcaption><p>Reference data architecture deployed within Joule  </p></figcaption></figure>

***

## Available enrichment implementations

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Reference Data</strong></mark></td><td>Use linked low latency data stores to enrich events with fresh reference data</td><td><a href="reference-data-enrichment.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Metrics</strong></mark></td><td>Enrich events with pre-calculated metrics to facilitate further pipeline processing</td><td><a href="metrics-enrichment.md"><mark style="color:orange;">Learn more</mark></a></td></tr></tbody></table>

