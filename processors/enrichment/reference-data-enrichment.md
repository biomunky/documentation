---
description: >-
  Enrichment of streaming events with external data is supported through the use
  of a low-latency data caching platform
---

# Reference Data Enrichment

***

Enriching event data with the latest reference data is crucial for processing real-time business insights. However, as reference data typically resides in a data mart or warehouse and changes slowly, integrating it into a high-throughput streaming platform can lead to processing latency challenges. This is often attributed to the I/O overhead induced by the network and storage layer.

Joule offers a solution to eliminate this latency by providing an embedded cache within the process. Upon Joule startup, developers can define the necessary datasets and customise the caching infrastructure's behaviour. This enterprise-level production approach supports advanced use cases, delivering real-time insights and triggering business decisions with agility.

#### Example enrichments cases

* Customer data bundle plan, credit rating etc;
* Company market rating, previous day stock sensitives etc;
* Cell tower signal coverage, machine utilisation scaling limits etc;
* Static Geofences for places of interest
* Dynamic business trigger parameters (i.e. SLAs, stock price levels...)
* General domain level static KPI / metrics

## Architecture

The Joule implementation architecture binds a Geode client cache within the process. This provides live data to be provided by a **Managed Service**. The service would provide the necessary upsert hooks from the database to the distributed cache when in turn would update the client cache using operational fucntions (i.e invildate, expire, GII, cache miss etc;)

<figure><img src="../../.gitbook/assets/reference data arch.png" alt=""><figcaption><p>Caching architecture implementation</p></figcaption></figure>

***

## Enricher DSL&#x20;

The enricher processor provide users to the ability to enrich an event with multiple data elements through the use of enhanced mapping.

### Enricher example

```yaml
enricher:
  fields:      
    deviceManufacturer:
      with values: [deviceManufacturer, year_released]
      by key: tac
      using: deviceStore

    model:
      with values: [modelNumber]
      by key: tac
      using: deviceStore

    contractedDataBundle:
      with values: [dataBundle]
      by query:  "select * from /userBundle where imsi = ?"
      query fields: [imsi]
      using: dataBundleStore

  stores:
    deviceStore:
      store name: mobiledevices

    dataBundleStore:
      store name: mobilecontracts
```

### Core Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>List of fields to populated with reference data using a look up criteria.</td><td>List of field to reference data criteria configurations </td><td>true</td></tr><tr><td>stores</td><td>List of stores to perform the reference data lookup</td><td>List of store configurations</td><td>true</td></tr></tbody></table>

## Field Configurations

For each additional field to be added to the event a configuration is requred. Below sets out the required DLS elements to be used.

### Core Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>with values</td><td>values to add to the field as a map of key value pairs</td><td>List of Strings</td><td>true</td></tr><tr><td>as object</td><td>Returned value is linked as a object to the event</td><td>Boolean</td><td>false</td></tr><tr><td>using</td><td>Store name to query</td><td>String</td><td>true</td></tr></tbody></table>



### Key based enrichment

Using the key based look up approach requires a target key value store to be defined for the criteria. Out of the box Joule provides a [Apache Geode](https://geode.apache.org/) connector which uses an embedded client cache.&#x20;

#### Provide return reference data as a linked map of key value pairs

```yaml
deviceManufacturer:
   with values: [deviceManufacturer, year_released]
   by key: symbol
   using: nasdaqIndexCompanies
```

#### Provide return reference data as a linked object

```
deviceInfo:
  as object: true
  by key: tac
  using: deviceStore
```

#### Attributes

<table><thead><tr><th width="168">Attribute</th><th width="392">Description</th><th width="174">Data Type</th></tr></thead><tbody><tr><td>by key</td><td>This uses the value within the passed event as a lookup key on the linked store</td><td>String</td></tr></tbody></table>

### Query based enrichment

Using the key based look up approach requires a target key value store to be defined for the criteria. Out of the box Joule provides a [Apache Geode](https://geode.apache.org/) connector which uses an embedded client cache.&#x20;

```yaml
contractedDataBundle:
    with values: [dataBundle]
    by query:  "select * from /userBundle where imsi = ?"
    query fields: [imsi]
    using: dataBundleStore
```

#### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="340">Description</th><th width="219">Data Type</th></tr></thead><tbody><tr><td>by query</td><td>OQL query </td><td>String</td></tr><tr><td>query fields</td><td>Event field values to be applied to the query</td><td>Ordered list of Strings</td></tr></tbody></table>

***

## Store Configurations

Provides the ability to define one or more linked data stores using a logical store name mapped to a set of configuration attributes.&#x20;

The example below uses the `nasdaqIndexCompanies` as a logical name to bind the reference data lookup criteria to be performed.&#x20;

```yaml
stores:
  deviceStore:
    store name: mobiledevices

  dataBundleStore:
    store name: mobilecontracts
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="425">Description</th><th width="160">Data Type</th></tr></thead><tbody><tr><td>stores</td><td>List of stores required for the enricher. Usage is local logical name mapped to <code>store name</code></td><td>String</td></tr><tr><td>store name</td><td>Actual store name specificed in the reference data store configuration</td><td>String</td></tr></tbody></table>

## Supported data stores

* Apache Geode
* S3
