# Remove attributes

Remove defined fields from the source event. This is ideal when there is a regulation requirement to process events without PII information at source.&#x20;

### Example

This process will remove `credit_card_number` and `address` fields from the source event ready for the next processor.

```yaml
field filter:
  fields: 
    - credit_card_number
    - address
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>List of fields to remove from the event</td><td>List&#x3C;String></td><td>true</td></tr></tbody></table>
