# Expression

This filter processor applies a custom Javascript expression to filter events. This is ideal when there is a need to fine tune the filtering process to the needs of the use case.

### Example

```yaml
filter:
  expression: (typeof symbol !== 'undefined' && symbol === 'IBM') ? true : false;
```

This will provide all IBM events to the next processors. Note top level StreamEvent attributes are not available within the scripting processing context.

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>expression</td><td>Javascript expression that returns a boolean value</td><td>String</td><td>true</td></tr></tbody></table>
