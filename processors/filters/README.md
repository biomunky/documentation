---
description: >-
  Filtering event based using a configurable criteria or a an expression.
  Example use cases is customer opt-out, missing data elements, out of range
  etc,.
---

# Filters

## Available filter implementations

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>By type</strong></mark></td><td>Filter events specifically typed from the processing pipeline</td><td><mark style="color:orange;">Learn more</mark></td></tr><tr><td><mark style="color:orange;"><strong>By  expression</strong></mark></td><td>Use Javascript expressions  to define custom filters.            </td><td>Learn more</td></tr><tr><td><mark style="color:orange;"><strong>By delta</strong></mark></td><td>Stateful filter processor that filters previous seen events</td><td><a href="send-on-delta.md"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Remove attributes</strong></mark></td><td>Remove sensitive attributes from an event</td><td><a href="remove-attributes.md"><mark style="color:orange;">Lean more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Drop all</strong></mark></td><td>Drop all incoming events in the processor</td><td><a href="drop-all.md"><mark style="color:orange;">Learn more</mark></a></td></tr></tbody></table>

