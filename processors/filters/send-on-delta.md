# Send on delta

This processor employs a highly efficient send-on-delta algorithm, leveraging user-defined fields to ascertain whether a new event exhibits changes in these specified fields. Only unique events that demonstrate alterations are forwarded to interconnected components.

### Example

```yaml
send on delta:
  fields:
    - symbol
    - bid
    - ask
  expected unique events: 100000
  fpp: 0.98
```

This will publish unseen `delta` events to the next processor.&#x20;

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>fields</td><td>Javascript expression that returns a boolean value</td><td>List&#x3C;String></td><td>true</td></tr><tr><td>expected unique events</td><td>Expected unique events. This must be calculated correctly otherwise false positives will be generated as the number approaches and breaches the provided number. Minimum number of events is 100 otherwise the filter cannot be created.</td><td>Long</td><td>true</td></tr><tr><td>fpp</td><td>The desired false positive probability. Must be positive and less than 1.0</td><td><p>Double</p><p>Default: 0.97</p></td><td>false</td></tr><tr><td>reset by time delay</td><td>Reset the bloom filter after a set number of seconds. </td><td><p>Long</p><p>Default: 0</p></td><td>false</td></tr><tr><td>reset by event count</td><td>Reset the bloom filter after a set number of events processed. </td><td><p>Integer</p><p>Default: 0</p></td><td>false</td></tr></tbody></table>

### Filter replacement

The processor uses a BloomFilter to determine if an event has been seen before. Therefore the filter is automatically updated when there is a high probability of false positives being generated.&#x20;

Any StreamEvent which has the deltaBreachedFPP field will signify a filter change once the event has been published.

#### Note

Both of the reset functions can be applied.
