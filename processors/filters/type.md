# Type

Filter events specifically typed from the processing pipeline. This reduces processing and memory overhead.

### Example

This process will filter out all ftx crypto events from the processing pipeline

```yaml
type filter:
  type: crypto
  subtype: ftx
  exclude: true
```

### Attributes

<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>type</td><td>Event type</td><td>String</td><td>true</td></tr><tr><td>subtype</td><td>Event subtype</td><td>String</td><td>true</td></tr><tr><td>exclude</td><td>Flag to switch event exclusion</td><td><p>Boolean</p><p>Default: true </p></td><td>false</td></tr></tbody></table>
