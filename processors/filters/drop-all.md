# Drop All

This filter processor drops all events. Consider using this in combination with the [Event Tap](../event-tap.md) processor.

### Example

```yaml
drop filter: {}
```
