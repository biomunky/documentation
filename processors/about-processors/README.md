---
description: >-
  Processors form the core of the Joule platform. A processor performs a
  distinct unique function when chained with others form a business use case
---

# About processors

***

## Overview

A use case core definition is based upon the processing pipeline. A pipeline is formed of one or more linked processors. This follows the functional programming paradigm whereby each processor performs a distinct process.

Events are processed in sequence with the final event either being presented to a select processor or  directly to connected transports.

<figure><img src="../../.gitbook/assets/pipeline.png" alt=""><figcaption><p>Pipeline forms a use case</p></figcaption></figure>

***

## Available processor types

Joule has provided a number of core processors for developers to leverage to build domain use case. Processors can be categorised in to various functional grouping that reflect there purpose within the platform.

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td><mark style="color:orange;"><strong>Filtering</strong></mark></td><td>Reduce stream processing overhead by filtering unnecessary events</td><td><a href="../filters/"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Enrichment</strong></mark></td><td>Enrich events using linked reference data, metrics and analytics</td><td><a href="../enrichment/"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Transformation</strong></mark></td><td>Transform event attribute values based upon a desired target state </td><td><a href="../transformation/"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Analytics</strong></mark></td><td>Gain insights through stream based analytic functions</td><td><a href="../analytics/"><mark style="color:orange;">Learn more</mark></a></td></tr><tr><td><mark style="color:orange;"><strong>Triggers</strong></mark></td><td>Apply rule based triggers for downstream business actions</td><td><a href="../triggers/"><mark style="color:orange;">Learn more</mark></a></td></tr></tbody></table>



***

## Observability and Metrics

Each OOTB processor record and presents event metrics to JMX monitoring platforms. This provides direct insight into how a streaming use case is performing.&#x20;

_Metrics available_

* Received
* Processed
* Failed
* Discarded
* Ignored
* Average processing latency

All of these metrics are enabled by default and presented as JMX beans on the standard monitoring ports.

### Enable JMX

Apply these settings to the joule execution path

```
-Dcom.sun.management.jmxremote.port=5000
-Dcom.sun.management.jmxremote.rmi.port=5000
```

For further information use the instructions in [Monitoring and Management Using JMX Technology](https://docs.oracle.com/javase/8/docs/technotes/guides/management/agent.html)
