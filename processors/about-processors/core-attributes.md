# Core Attributes

Every processor that inherits from the base class `AbstractProcessor` , using the Joule SDK, will gain these core features.



<table><thead><tr><th width="193">Attribute</th><th width="217">Description</th><th width="219">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Uniquie name of processor. </td><td><p>String</p><p>Default: Random UUID</p></td><td>false</td></tr><tr><td>enabled</td><td>Toogle to enable processor processing</td><td><p>Boolean</p><p>Default: true</p></td><td>false</td></tr><tr><td>properties</td><td>Processor specific properties</td><td>Map</td><td>false</td></tr><tr><td>clone event</td><td>Toogle to clone event for processing. Note this will cause significant memory overhead for high load use cases</td><td><p>Boolean</p><p>Default: false</p></td><td>false</td></tr><tr><td>stores</td><td>External data stores can be attached, useful for analytics, enrichment, filtering and transformation use cases</td><td></td><td>false</td></tr><tr><td>latency trigger</td><td>Number of events processed to trigger the average processing calculation. A value less than 1 switches off the calculation.</td><td><p>Integer</p><p>Default: 1000</p></td><td>false</td></tr></tbody></table>
