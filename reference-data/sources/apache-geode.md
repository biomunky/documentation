---
description: >-
  Apache Geode provides a database-like consistency model, reliable transaction
  processing and a shared-nothing architecture to maintain very low latency
  performance with high concurrency processing
---

# Apache Geode

#### _Driver details_

```groovy
org.apache.geode:geode-core:1.15.1
```

***

## Overview

Joule uses Geode to as as a high performance in-memory key-value store to support advanced  stream processing functions at low latency. This architecture pattern has been production deployed and leveraged within large enterprise solutions over 15 years.

Storage regions can be primed on startup using either a custom query or a getAll function otherwise data is fetched on cache misses. Furthermore for cached data elements, data updates are immediately propagated from the connected distributed cluster to the Joule process thereby enabling up-to-date reference data.

#### Example configuration

```yaml
geode stores:
    name: us markets
    connection:
        locator address: 192.168.86.39
        locator port: 41111
    stores:
      nasdaqIndex:
        region: nasdaq-companies
        keyClass : java.lang.String
        gii: true
      holidays:
        region: us-holidays
        keyClass : java.lang.Integer
```



### Core attributes

Need to refactor by adding connection spec

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Logical name of reference data set for given target connection</td><td>String</td><td>true</td></tr><tr><td>locator address </td><td>Locator process IP address</td><td>String<br>Default: localhost</td><td>true</td></tr><tr><td>locator port</td><td>The port the cluster locator listens for cluster and client connections</td><td>Integer<br>Default: 41111</td><td>true</td></tr><tr><td>stores</td><td>Map of logical names to region configurations. See Region attributes</td><td>List of logical storage name to Geode storage region</td><td>true</td></tr></tbody></table>
