---
description: >-
  The storage flexibility of S3 provides the developer that ability to leverage
  existing data assets within a streaming context
---

# MinIO S3

#### _Driver details_

```groovy
io.minio:minio:8.5.4
```

***

## Overview

This storage type is suited for very slow changing data which can be cached and used within a streaming context, therefore this data would have a low update frequency cycle.

#### Example configuration

```
minio stores:
    name: Telco ML Models
    connection:
      endpoint: "https://localhost"
      port: 9000
      tls: false
      credentials:
        access key: "XXXXXXX"
        secret key: "YYYYYYYYYYYYYYY"
    stores:
      predictors:
        bucketId: models
        initial version Id: 12345
        download dir: /home/joule/telco-models/tmp
```

This example uses the MinIO S3 connector to load a ML model on start up using the specified credentials and bucket details. The reference implementation is the `pmml predictor` processor, [see documentation](../../processors/machine-learning/online-predictive-analytics.md) for further details.&#x20;

### Core attributes

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>name</td><td>Logical name of reference data set for given target connection</td><td>String</td><td>true</td></tr><tr><td>connection </td><td>Connection details</td><td>See <a href="../../connector-catalog/sources/minio-s3.md#connection-attributes">Connection  configuration</a></td><td>true</td></tr><tr><td>stores</td><td>Map of logical names to bucket level configurations. See <a href="minio-s3.md#bucket-attributes">bucket attributes section</a></td><td>Map</td><td>true</td></tr></tbody></table>

### Bucket Attributes

<table><thead><tr><th width="171">Attribute</th><th width="280">Description</th><th width="188">Data Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>bucketId</td><td>S3 bucket where the reference data resides </td><td>String</td><td>true</td></tr><tr><td>initial version id</td><td>The version id to use when priming the system. This is a global version for the given bucket otherwise the latest version of the object is used</td><td>String</td><td>false</td></tr><tr><td>download dir</td><td>Local path for bucket objects to be downloaded before processing starts </td><td>String<br>Default: ./tmp</td><td>false</td></tr></tbody></table>

