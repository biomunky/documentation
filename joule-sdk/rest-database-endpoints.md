# Rest Database Endpoints

RestAPI publisher transport provides a number of endpoints to access data ingested and processed within the Joule process. Raw and processed events are stored within the internal SQL database which are then made accessible to users via rest endpoints.

#### Swagger Support

All endpoints have OpenAPI support via swagger docs url

```
http://<host>:7070/swagger
```

<figure><img src="../../../.gitbook/assets/Screenshot 2022-10-24 at 13.51.01.png" alt=""><figcaption></figcaption></figure>

#### _Driver details_

```groovy
io.javalin:javalin:5.6.3
```

## Supported endpoints

Oveview of supported rest endpoints. All data is returned as Json StreamEvents with the exception of export endpoint which is user definable.

```
// List available schema and table queries
http://<host>:7070/joule/query/list

// Query schema table
http://<host>:7070/joule/query/{schema}/{table}

// Export schema table as a typed file
http://<host>:7070/joule/export/{schema}/{table}/{type}

// Delete schema table
http://<host>:7070/joule/delete/{schema}/{table}
```

### List available queries

Get a list of available query tables.

#### Endpoint

```
// Some code
http://localhost:7070/joule/query/list
```

### Query table

Query an internal SQL table with or without a date time range based of ingestTime constraint. The returned query resultset is constrainted to a specified page size.

#### Endpoint

```
http://localhost:7070/joule/query/{schema}/{table}
```

#### Parameters

<table><thead><tr><th width="121">Attribute</th><th width="331">Description</th><th width="162">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Database schema is use</td><td>Path</td><td>true</td></tr><tr><td>table</td><td>Table to export from</td><td>Path</td><td>true</td></tr><tr><td>from</td><td><p>DateTime format</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>false</td></tr><tr><td>to</td><td><p>DateTime</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>false</td></tr><tr><td>offset</td><td>Event page offset</td><td>Integer</td><td>true</td></tr><tr><td>pageSize</td><td><p>Number of events to return per page</p><p>Default: 1000</p></td><td>Integer</td><td>false</td></tr></tbody></table>



### Export data

Export event data to a typed file using a date time range constraint, uses ingestTime field to restrict the query result. Parquet and CSV files are generated based upon user request parameter.

#### Endpoint

```
http://localhost:7070/joule/export/{schema}/{table}
```

#### Parameters

<table><thead><tr><th width="121">Attribute</th><th width="331">Description</th><th width="162">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Database schema is use</td><td>Path</td><td>true</td></tr><tr><td>table</td><td>Table to export from</td><td>Path</td><td>true</td></tr><tr><td>type</td><td>Options: parquet, csv</td><td>Query</td><td>false</td></tr><tr><td>from</td><td><p>DateTime format</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>true</td></tr><tr><td>to</td><td><p>DateTime</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>true</td></tr><tr><td>path</td><td><p>Destination directory and file name</p><p>e.g. /tmp/events.parquet</p></td><td>Query</td><td>true</td></tr></tbody></table>

### Delete data

Delete event data from an internal database table using a date time crirtera. The query uses ingestTime field to restrict the data deleted.

#### Endpoint

```
http://localhost:7070/joule/delete/{schema}/{table}/{type}
```

#### Parameters

<table><thead><tr><th>Attribute</th><th width="331">Description</th><th width="162">Type</th><th data-type="checkbox">Required</th></tr></thead><tbody><tr><td>schema</td><td>Database schema is use</td><td>Path</td><td>true</td></tr><tr><td>table</td><td>Table to export from</td><td>Path</td><td>true</td></tr><tr><td>from</td><td><p>DateTime format</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>true</td></tr><tr><td>to</td><td><p>DateTime</p><p>e.g. YYYY-MM-dd hh:mm:ss</p></td><td>Query</td><td>true</td></tr></tbody></table>

#### Default configuration

```yaml
http://<host>:7070/joule/
```
