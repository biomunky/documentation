---
description: >-
  An event drives all processing. At its core Joule defines a StreamEvent that
  is passed from one component to another until it is ejected to consumers.
---

# StreamEvent

Understanding the structure and how to use this datatype will enable you to develop custom components that are optimised for processing efficiency while being flexible enough to work with. &#x20;

{% hint style="success" %}
The StreamEvent provides the vehicle to transport and process data throughout the platform
{% endhint %}

## Key Features

* Typed by source system
* Timestamped by event occurrence at source or at time of platform ingestion
* Changes trackable by time, value and by affecting processor
* Cloneable, deep copy, to preseve transaction isolation within processing context
* Serializable for extenal consumption

## Attributes

<table><thead><tr><th width="148">Attribute</th><th width="410.3333333333333">Description</th><th>Type</th></tr></thead><tbody><tr><td>uuid</td><td>Unique identifier assigned by the platform to enable tracking</td><td>UUID</td></tr><tr><td>type</td><td>Parent type of the event e.g. market_data</td><td>String</td></tr><tr><td>subType</td><td>Finer grain type of the event e.g. aim </td><td>Object</td></tr><tr><td>eventTme</td><td>Actual time the event occured within the source system. This is set by the user.</td><td>Long</td></tr><tr><td>IngestTime</td><td>Actual time the event was ingested by Joule process. This is automatically assigned at point of object creation.</td><td>Long</td></tr><tr><td>dictionary</td><td>Store of all key event attributes.</td><td>Map&#x3C;String,Object></td></tr><tr><td>callChain</td><td>Tracking store of every change made to the event as it passes through the processing pipeline. This is managed by the Joule platform. </td><td>Map&#x3C;Tuple&#x3C;UUID, String>, Long></td></tr></tbody></table>



{% hint style="info" %}
StreamEvent can be located under the following package

```java
com.fractalworks.streams.core.data.streams
```
{% endhint %}

## Key Methods

### Adding

To add new attributes to an event there are two methods which can be used. If a null value has been passed this will be converted to a String `null` value.

```java
// Add field and value using the default UUID
public void addValue(String field, Object value);

// Add field and value using the passed UUID
public void addValue(UUID srcUUID, String field, Object value);
```

### Updating

Attributes can be updated in place.  If a null value has been passed this will be converted to a String `null` value.

```java
// Update field and value using the passed UUID
public void replaceValue(UUID srcUUID, String field, Object value);
```

### Get value

Either a single attribute or a the whole set of attributes values be returned to the caller

```java
// Get value for field
public Object getValue(String field);

// Get a shallow copy of the field value dictionary
public Map<String, Object> getDictionary();
```

### Remove

Tow methods exists to remove fields from the event.&#x20;

```java
// Remove a single field from the event
public void removeValue(UUID srcUUID, String field);

// Remove all stored field data
public void removeAll();
```

### Cloning

A deep copy method is provided to return a new StreamEvent from the event.

```java
// Get a deep copy of this eventfav
public StreamEvent deepCopy();
```

### Get call chain

To get the history of changes applied to the event use the below method.

```java
// Get the history of changes applied to the event
public Map<Triple<UUID, String,Object>, Long> getCallChain();
```

