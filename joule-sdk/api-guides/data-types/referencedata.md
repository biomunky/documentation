---
description: >-
  Reference data provides additional context to support advance stream
  processing use case
---

# ReferenceData

Understanding the structure and how to use this datatype will enable you to develop custom enrichments and analytics.&#x20;

{% hint style="success" %}
The ReferenceDataObject provides a generic data structure for reference data to be store and queried against
{% endhint %}

## ReferenceDataObject

Joule provides a base implementation for the storage of a reference data element. The class extends the HashMap\<String, Object> class and therefore has all the benefits of a Map with a couple of extensions.

## Attributes

<table><thead><tr><th width="135">Attribute</th><th width="428">Description</th><th>Type</th></tr></thead><tbody><tr><td>key</td><td>Unique reference data key used when performing query lookups</td><td>Object</td></tr><tr><td>data</td><td>Custom data to be stored outside of the Map structure</td><td>Object</td></tr></tbody></table>

## Key Methods

### Key

To add new attributes to an event there are two methods which can be used. If a null value has been passed this will be converted to a String `null` value.

```java
// Set lookup key
public void setKey(Object key);

// Get lookup key
public Object getKey();
```

### Data

o add new attributes to an event there are two methods which can be used. If a null value has been passed this will be converted to a String `null` value.

```java
// Set custom data object
public void setData(Object data);

// Get the custom data object
public Object getData();
```
