# Plugins API

## Requirements

See [Setting up the environment](../setting-up-the-environment.md) documentation

{% hint style="info" %}
Note all gradle commands must be execute at the root of the project directory
{% endhint %}

## Development steps

### Interface

```java
public interface CustomPlugin {

    /**
     * Initialise sets up the user plugin
     *
     * @throws CustomUserPluginException
     */
    default void initialize() throws CustomUserPluginException {
    }

    /**
     * Validate plugin setting
     *
     * @throws InvalidSpecificationException
     */
    default void validate() throws InvalidSpecificationException {
    }

    /**
     * Set plugin properties
     *
     * @param properties plugin properties
     */
    @JsonProperty(value = "properties")
    default void setProperties(Properties properties){
    }
}
```
