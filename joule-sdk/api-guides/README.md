---
description: >-
  Joule provide four core APIs that enable developers to extend the platform
  with custom domain artefacts
---

# API Documentation

## Available APIs

Joule has five core APIs which are intended for different uses.The table below outlines their descriptions and example use cases.

<table><thead><tr><th width="154.33333333333331">API</th><th width="367">Description</th><th>Use case</th></tr></thead><tbody><tr><td>Connector</td><td>Enables users programmatically develop custom data connectors</td><td>When there is a need to consume/publish events from a non-supported connector</td></tr><tr><td>Processor</td><td>Enables users programmatically develop custom event processors which exploit Joules existing features</td><td>Advanced analytic use cases where additional data processing is required</td></tr><tr><td>Analytics</td><td>A set of APIs that enables developers to define metrics, complex SQL queries, and ability to developer custom advanced analytics</td><td>Support advanced analytics, machine learning, dynamic logic processing etc,.</td></tr><tr><td>Metrics</td><td>Enables users to embed computed metrics within custom processors</td><td>Support advanced analytics, machine learning, dynamic logic processing etc,.</td></tr><tr><td>SQL</td><td>Enables users to leverage the power of ANSI SQL within custom processors </td><td>Execute complex queries that may span across metrics, reference data and additional data</td></tr><tr><td>Plugins</td><td></td><td>Developing feature engineering, data quality etc,. functions</td></tr><tr><td>Rest</td><td>Access processed data </td><td>Auditing</td></tr></tbody></table>



