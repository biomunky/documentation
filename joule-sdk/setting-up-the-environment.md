# Setting up the environment

{% hint style="info" %}
**Note:** Joule tries to use the most up-to-date releases of dependencies and tools to ensure the platform captures the latest technologies and bug fixes the industry offers. Follow your operating system installation instructions for required packages.
{% endhint %}

### JDK

Install OpenJDK 17

```
openjdk 17.0.5 2022-10-18
OpenJDK Runtime Environment GraalVM CE 22.3.0 (build 17.0.5+8-jvmci-22.3-b08)
OpenJDK 64-Bit Server VM GraalVM CE 22.3.0 (build 17.0.5+8-jvmci-22.3-b08, mixed mode, sharing)
```

&#x20;Preferred implementation [GraalVM CE 22.3.0](https://www.graalvm.org/downloads/)

### Install Languages

To use the dynamic scripting processors with preferred languages these need to be installed using the below commands

```
gu available
gu install js
gu install nodejs
gu install python
```

Official [documentation](https://docs.oracle.com/en/graalvm/enterprise/20/docs/reference-manual/graalvm-updater/)

### Gradle

Install Gradle 7.6&#x20;

```
------------------------------------------------------------
Gradle 7.6
------------------------------------------------------------

Build time:   2022-11-25 13:35:10 UTC
Revision:     daece9dbc5b79370cc8e4fd6fe4b2cd400e150a8

Kotlin:       1.7.10
Groovy:       3.0.13
Ant:          Apache Ant(TM) version 1.10.11 compiled on July 10 2021
JVM:          19.0.1 (Azul Systems, Inc. 19.0.1+10)
OS:           Mac OS X 13.0.1 x86_64
```

{% embed url="https://gradle.org/releases/" %}

### Git

Install the latest version of [git](https://docs.gitlab.com/ee/topics/git/how\_to\_install\_git/) to enable you to clone Joule repositories.&#x20;
