# Table of contents

* [Joule Documentation](README.md)
* [Release Notes](release-notes/README.md)
  * [Streaming analytics enhancements](release-notes/streaming-analytics-enhancements.md)
  * [Predictive stream processing](release-notes/predictive-stream-processing.md)
  * [Contextual SQL based metrics](release-notes/contextual-sql-based-metrics.md)

## Introducing Joule

* [Overview](introducing-joule/overview.md)
* [Key Features](introducing-joule/key-features.md)
* [FAQ](introducing-joule/faq.md)

## Use Cases

* [Overview](use-cases/overview.md)
* [📱 Telco](use-cases/telco/README.md)
  * [Environment installation](use-cases/telco/environment-installation.md)
  * [IMSI Opt-Out](use-cases/telco/imsi-opt-out.md)
  * [Enrich event](use-cases/telco/enrich-event.md)
  * [Anonymise IMSI](use-cases/telco/anonymise-imsi.md)
  * [Location tracking](use-cases/telco/location-tracking.md)
  * [Geospatial marketing](use-cases/telco/geospatial-marketing.md)
  * [Basic analytics](use-cases/telco/basic-analytics.md)
* [💵 Banking](use-cases/banking/README.md)
  * [Environment installation](use-cases/banking/environment-installation.md)
  * [Tumbling window analytics](use-cases/banking/tumbling-window-analytics.md)
  * [Sliding window analytics](use-cases/banking/sliding-window-analytics.md)
  * [User defined analytics](use-cases/banking/user-defined-analytics.md)
  * [Bollinger bands](use-cases/banking/bollinger-bands.md)
  * [Aggregates and user defined time based window functions](use-cases/banking/aggregates-and-user-defined-time-based-window-functions.md)
  * [Time based metrics](use-cases/banking/time-based-metrics.md)

## Getting started

* [Quickstart](getting-started/quickstart.md)
* [Core concepts](getting-started/core-concepts.md)
* [Install Joule](getting-started/install-joule/README.md)
  * [Install with Docker](getting-started/install-joule/install-with-docker.md)
  * [Install from source](getting-started/install-joule/install-from-source.md)
  * [Install Joule examples](getting-started/install-joule/install-joule-examples.md)
* [Joule Configuration](getting-started/joule-configuration.md)
* [Joule CLI](getting-started/joule-cli.md)

## Build Joule projects

* [Low Code DSL](build-joule-projects/low-code-dsl.md)
* [Functional components](build-joule-projects/functional-components.md)
* [Build your app DAG](build-joule-projects/build-your-app-dag/README.md)
  * [Sources](build-joule-projects/build-your-app-dag/sources.md)
  * [Use cases overview](build-joule-projects/build-your-app-dag/use-cases-overview/README.md)
    * [Initialisation](build-joule-projects/build-your-app-dag/use-cases-overview/initialisation.md)
    * [Processing unit](build-joule-projects/build-your-app-dag/use-cases-overview/processing-unit.md)
    * [Emit](build-joule-projects/build-your-app-dag/use-cases-overview/emit.md)
    * [Group by](build-joule-projects/build-your-app-dag/use-cases-overview/group-by.md)
    * [Telemetry Auditing](build-joule-projects/build-your-app-dag/use-cases-overview/telemetry-auditing.md)
  * [Sinks](build-joule-projects/build-your-app-dag/sinks.md)

## Connector Catalog

* [Sources](connector-catalog/sources/README.md)
  * [Kafka](connector-catalog/sources/kafka.md)
  * [RabbitMQ](connector-catalog/sources/rabbitmq.md)
  * [MQTT](connector-catalog/sources/mqtt.md)
  * [MinIO S3](connector-catalog/sources/minio-s3.md)
  * [REST](connector-catalog/sources/rest.md)
  * [File Watcher](connector-catalog/sources/file-watcher.md)
* [Sinks](connector-catalog/sinks/README.md)
  * [Kafka](connector-catalog/sinks/kafka.md)
  * [MQTT](connector-catalog/sinks/mqtt.md)
  * [RabbitMQ](connector-catalog/sinks/rabbitmq.md)
  * [Websocket](connector-catalog/sinks/websocket.md)
  * [SQL Databases](connector-catalog/sinks/sql-databases.md)
  * [MinIO S3](connector-catalog/sinks/minio-s3.md)
  * [InfluxDB](connector-catalog/sinks/influxdb.md)
  * [MongoDB](connector-catalog/sinks/mongodb.md)
  * [File](connector-catalog/sinks/file.md)
  * [Geode](connector-catalog/sinks/geode.md)
  * [Console](connector-catalog/sinks/console.md)
* [Serialization](connector-catalog/serialization/README.md)
  * [Deserializers](connector-catalog/serialization/deserializers.md)
  * [Serializers](connector-catalog/serialization/serializers.md)

## Processors

* [About processors](processors/about-processors/README.md)
  * [Core Attributes](processors/about-processors/core-attributes.md)
* [Filters](processors/filters/README.md)
  * [Type](processors/filters/type.md)
  * [Expression](processors/filters/expression.md)
  * [Send on delta](processors/filters/send-on-delta.md)
  * [Remove attributes](processors/filters/remove-attributes.md)
  * [Drop All](processors/filters/drop-all.md)
* [Enrichment](processors/enrichment/README.md)
  * [Reference Data Enrichment](processors/enrichment/reference-data-enrichment.md)
  * [Metrics Enrichment](processors/enrichment/metrics-enrichment.md)
* [Transformation](processors/transformation/README.md)
  * [Field Tokenizer](processors/transformation/field-tokenizer.md)
  * [Obfuscation](processors/transformation/obfuscation/README.md)
    * [Encryption](processors/transformation/obfuscation/encryption.md)
    * [Masking](processors/transformation/obfuscation/masking.md)
    * [Bucketing](processors/transformation/obfuscation/bucketing.md)
    * [Redaction](processors/transformation/obfuscation/redaction.md)
* [Machine Learning](processors/machine-learning/README.md)
  * [Feature Engineering](processors/machine-learning/feature-engineering/README.md)
    * [Scripting](processors/machine-learning/feature-engineering/scripting.md)
    * [Scaling](processors/machine-learning/feature-engineering/scaling.md)
    * [Transform](processors/machine-learning/feature-engineering/transform.md)
  * [Online predictive analytics](processors/machine-learning/online-predictive-analytics.md)
  * [Model Audit](processors/machine-learning/model-audit.md)
  * [Model Management](processors/machine-learning/model-management.md)
* [Analytics](processors/analytics/README.md)
  * [Metrics Engine](processors/analytics/metrics-engine/README.md)
    * [Create a metric](processors/analytics/metrics-engine/create-a-metric.md)
    * [Use Metrics](processors/analytics/metrics-engine/use-metrics.md)
    * [Manage Metrics](processors/analytics/metrics-engine/manage-metrics.md)
  * [Window Analytics](processors/analytics/window-analytics/README.md)
    * [Tumbling](processors/analytics/window-analytics/tumbling.md)
    * [Sliding](processors/analytics/window-analytics/sliding.md)
  * [Scripting](processors/analytics/scripting.md)
  * [Custom Analytics](processors/analytics/custom-analytics.md)
  * [Geospatial](processors/analytics/geospatial/README.md)
    * [Entity Geo Tracker](processors/analytics/geospatial/entity-geo-tracker.md)
    * [Geofence Occupancy Trigger](processors/analytics/geospatial/geofence-occupancy-trigger.md)
    * [Spatial Index](processors/analytics/geospatial/spatial-index.md)
  * [HyperLogLog](processors/analytics/hyperloglog.md)
* [Triggers](processors/triggers/README.md)
  * [Change Data Capture](processors/triggers/change-data-capture.md)
  * [Declarative Rules](processors/triggers/declarative-rules.md)
* [Event Tap](processors/event-tap.md)

## Reference Data

* [About Reference Data](reference-data/about-reference-data.md)
* [Sources](reference-data/sources/README.md)
  * [MinIO S3](reference-data/sources/minio-s3.md)
  * [Apache Geode](reference-data/sources/apache-geode.md)

## Joule SDK

* [API Guides](joule-sdk/api-guides/README.md)
  * [Connectors API](joule-sdk/api-guides/connectors-api.md)
  * [Processors API](joule-sdk/api-guides/processors-api.md)
  * [Analytics API](joule-sdk/api-guides/analytics-api/README.md)
    * [Create custom metrics](joule-sdk/api-guides/analytics-api/create-custom-metrics.md)
    * [Define analytics](joule-sdk/api-guides/analytics-api/define-analytics.md)
    * [SQL queries](joule-sdk/api-guides/analytics-api/sql-queries.md)
  * [Plugins API](joule-sdk/api-guides/plugins-api.md)
  * [Data Types](joule-sdk/api-guides/data-types/README.md)
    * [StreamEvent](joule-sdk/api-guides/data-types/streamevent.md)
    * [ReferenceData](joule-sdk/api-guides/data-types/referencedata.md)
  * [Utilities](joule-sdk/api-guides/utilities/README.md)
    * [File processing](joule-sdk/api-guides/utilities/file-processing.md)
* [Setting up the environment](joule-sdk/setting-up-the-environment.md)
* [Rest Database Endpoints](joule-sdk/rest-database-endpoints.md)
* [Build and deploy](joule-sdk/build-and-deploy.md)

## Reference

* [Monitoring](reference/monitoring.md)
* [SQL](reference/sql.md)
* [Change history](reference/change-history.md)
* [MinIO S3 Backup](reference/minio-s3-backup.md)
